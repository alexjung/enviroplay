<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!--<link rel="icon" href="../../favicon.ico">-->

    <title><?php echo $title; ?></title>

    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="css/enviroplay.min.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body role="document">

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo $rootDirPath; ?>index.php">EnviroPlay</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
                <li<?php if ($scriptBaseName == 'index'){ echo ' class="active"'; } ?>><a href="<?php echo $rootDirPath; ?>index.php">Home</a></li>
                <li<?php if ($scriptBaseName == 'about'){ echo ' class="active"'; } ?>><a href="about.php">About</a></li>
                <li<?php if ($scriptBaseName == 'contact'){ echo ' class="active"'; } ?>><a href="<?php echo $rootDirPath; ?>contact.php">Contact</a></li>
                <?php
                if ($isUserLoggedIn){
                    ?>
                    <li class="dropdown<?php if ($scriptBaseName == 'quiz'){ echo ' active'; } ?>">
                        <a href="quiz.php" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Game Level <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <?php /* replace grade-id-# in class parameter with appropriate ID if these change */ ?>
                            <li><a href="quiz.php" class="navbar-quiz-nav grade-id-4">Grades K-3</a></li>
                            <li><a href="quiz.php" class="navbar-quiz-nav grade-id-3">Grades 4-6</a></li>
                            <li><a href="quiz.php" class="navbar-quiz-nav grade-id-2">Grades 7-8</a></li>
                            <li><a href="quiz.php" class="navbar-quiz-nav grade-id-1">High School</a></li>
                        </ul>
                    </li>
                <?php
                }
                ?>
            </ul>
        </div>
    </div>
</nav>
<form id="navbar-quiz-form" action="<?php echo $rootDirPath; ?>quiz.php" style="margin:0;" method="post">
    <input type="hidden" name="gradelevelid" id="navbar-quiz-gradelevelid" value="1">
</form>

<div class="container msg-container" style="margin-top:1em;">
    <?php
    /* check to see if any system messages exist; messages are and array of arrays, e.g.:
        [
            [
                'typeOfMessage', // could be success, info, warning, danger
                'textOfMessage'
            ]
        ];
    */
    foreach($_SESSION['systemMessage'] as $msg) {
        ?>
        <div class="alert alert-<?php echo $msg[0]; ?> alert-dismissible" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <?php echo $msg[1]; ?>
        </div>
    <?php
    }
    // remove message from memory
    $_SESSION['systemMessage'] = [];
    ?>
</div>