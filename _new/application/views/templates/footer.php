<footer class="footer">
    <div class="container">
        <?php
        if($isUserLoggedIn) {
            echo '<div class="username">';
            switch($scriptBaseName){
                case "index":
                    echo '<a href="includes/logout.php"><button class="btn btn-default">Logout</button></a>';
                    break;

                case "quiz":
                case "quiz-ng":
                    echo '<a href="results.php"><button class="btn btn-default">Quit Quiz</button></a>';
                    break;

                case "admin":
                    echo '<a href="../includes/logout.php"><button class="btn btn-default">Logout</button></a> <a href="../index.php"><button class="btn btn-default">Home</button></a>';
                    break;
            }

            echo '        <div class="username-box">' . $_SESSION["user"][$_SESSION['userIdx']]["username"] . '</div>
            </div>';
        }
        ?>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/ie10-viewport-bug-workaround.min.js"></script>
<script src="js/main.min.js"></script>
<?php
if (isset($pageJs)){
    echo $pageJs;
}
?>
</body>
</html>