var enviroplay = {
    setColumnHeights: function(){
        if ($(window).width() >= 992) {
            $(".equalizer").each(function () {
                var heights = $(this).find(".watch").map(function () {
                        return $(this).height();
                    }).get(),

                    maxHeight = Math.max.apply(null, heights);
                $(".watch").height(maxHeight);
            });
        }
        else {
            $(".watch").height('auto');
        }
    }
};
$(document).ready(function(){
    enviroplay.setColumnHeights();
    $(window).resize(function() {
        enviroplay.setColumnHeights();
    });

    $('.navbar-quiz-nav').click(function(e){
        e.preventDefault();

        var classNames = $(this).attr('class').split(" ");
        for ( var i = 0, l = classNames.length; i<l; ++i ) {
            if (classNames[i].indexOf('grade-id-') === 0){
                var newVal = classNames[i].replace('grade-id-', '');
                break;
            }
        }
        $('#navbar-quiz-gradelevelid').val(newVal);
        $('#navbar-quiz-form').submit();
    })
});