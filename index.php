<?php
require_once 'includes/utilityFunctions.php';

$pageTitle = "EnviroPlay";

// create placeholder variable for "Player #" string
$nextPlayer = "";

// create 1 player/2 player buttons
$playerStart = <<<PLAYERSTART
<div class="clearfix" style="text-align:center;margin-top:2em;">
                    <a href="playerStart.php?idx=0"><button type="button" class="btn btn-default one-player">1 Player</button></a>
                    <a href="playerStart.php?idx=1" style="margin-left:2em;"><button type="button" class="btn btn-default two-player">2 Players</button></a>
                </div>
PLAYERSTART;

require_once 'includes/template/header.php';
?>
<div class="container" role="main">
    <div class="row equalizer">
        <div class="col-sm-6 col-md-4">
            <div class="well well-lg watch">
<?php
$numPlayers = 0;
// if any users are logged in, display their names
if ($isUserLoggedIn):
    $nextPlayer = ' (Player #' . (count($_SESSION['user']) + 1) . ')';
    $numPlayers = count($_SESSION['user']);
    echo "                <p>Welcome ";

    for ($i = 0; $i < $numPlayers; $i++){
        if ($i > 0 && $i != $numPlayers - 1){
            echo ", ";
        }
        else if ($i > 0 && $i == $numPlayers - 1) {
            echo " and ";
        }
        echo $_SESSION['user'][$i]['username'];
        if ($i == $numPlayers - 1){
            echo "!";
        }
    }
    echo "                </p>";
endif;

// if max # of players reached, do not display sign in form
if ($numPlayers < QUIZ_MAX_PLAYERS):
    if ($isUserLoggedIn):
?>
                <p>There can be up to <?php echo convert_or_keep_number_to_word(QUIZ_MAX_PLAYERS); ?> players. Will another player join you?</p>
<?php
    else:
?>
                <h2 class="first">Sign in</h2>
<?php
    endif;
?>
                <form name="signin" method="post" action="includes/login.php">
                    <div class="form-group"">
                        <label for="myusername" class="sr-only">Username<?php echo $nextPlayer; ?></label>
                        <input type="text" id="myusername" name="myusername" placeholder="Username<?php echo $nextPlayer; ?>" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="mypassword" class="sr-only">Password</label>
                        <input type="password" id="mypassword" name="mypassword" placeholder="Password<?php echo $nextPlayer; ?>" class="form-control">
                        <small><a href="forgotPassword.php">forgot username/password</a></small>
                    </div>

                    <p class="last">
                        <button type="submit" class="btn btn-default">Sign in</button>
                        <a href="register.php"><button type="button" class="btn btn-lg btn-link">Register</button></a>
                    </p>
                </form>
<?php
endif;
?>
            </div>
        </div>
<?php
if ($isUserLoggedIn):
?>
        <div class="col-sm-6 col-md-3">
            <div class="row-same-height well well-lg watch" style="text-align:center;">
                <h3 class="first" style="margin-bottom:1em;">Select Game Level</h3>

                <form method="post" action="playerStart.php">
                    <p>
                        <button type="submit" class="btn btn-lg btn-default">Grades K-3</button>
                        <input type="hidden" name="gradelevelid" id="gradelevelid" value="4">
                    </p>
                </form>
                <form method="post" action="playerStart.php">
                    <p>
                        <button type="submit" class="btn btn-lg btn-default">Grades 4-6</button>
                        <input type="hidden" name="gradelevelid" id="gradelevelid" value="3">
                    </p>
                </form>
                <form method="post" action="playerStart.php">
                    <p>
                        <button type="submit" class="btn btn-lg btn-default">Grades 7-8</button>
                        <input type="hidden" name="gradelevelid" id="gradelevelid" value="2">
                    </p>
                </form>
                <form method="post" action="playerStart.php">
                    <p>
                        <button type="submit" class="btn btn-lg btn-default">High School</button>
                        <input type="hidden" name="gradelevelid" id="gradelevelid" value="1">
                    </p>
                </form>
            </div>
        </div>

        <div class="instructions col-sm-12 col-md-5">
            <div class="well well-lg watch">
                <h2 class="first">How to Play!</h2>

                <p>This is the area where you explain how to play.  This is the area where you explain how to play.  This is the area where you explain how to play. Test</p>

                <?php echo $playerStart; ?>
            </div>
        </div>
<?php
else:
?>
        <div class="instructions col-sm-6 col-md-8">
            <div class="well well-lg watch">
                <h2 class="first">Welcome!</h2>

                <p>This is the area where we write intro text about the games. This is the area where we write intro text about the games. This is the area where we write intro text about the games.</p>

                <?php echo $playerStart; ?>
            </div>
        </div>
<?php
endif;
?>
    </div>
</div>

<?php
require_once 'includes/template/footer.php';
?>
