<?php
require_once 'includes/utilityFunctions.php';

$pageTitle = "EnviroPlay";

// init gradelevilid if first submit to this page
if(isset($_POST['gradelevelid'])) {
    $_SESSION['gradelevel'] = $_POST['gradelevelid'];
}
// init topic if first submit to this page
if(isset($_POST['topic'])) {
    if ($_POST['topic'] != ''){
        // form val passed in as comma-delimited list; convert to array
        $_SESSION['topics'] = explode(",", $_POST['topic']);
    }
    else {
        // treat empty string as a "select all topics"; get topics for current grade level from db
        $qTopics = DB::query('SELECT DISTINCT rt.topic_area_id FROM question q, ref_topic_area rt WHERE q.level_id = %i AND q.topic_area_id = rt.topic_area_id ORDER BY rt.topic_area', $_SESSION['gradelevel']);

        $_SESSION['topics'] = [];
        // loop over DB results and save topic ids
        foreach($qTopics as $i => $key){
            array_push($_SESSION['topics'], $qTopics[$i]['topic_area_id']);
        }
    }
}

if (isset($_GET['reset'])){
    for ($i = 0; $i < count($_SESSION['user']); $i++){
        $_SESSION['user'][$i]['questionIDs'] = [];
        $_SESSION['user'][$i]['questionIncorrect'] = [];
        $_SESSION['user'][$i]['questionCount'] = 0;
        $_SESSION['user'][$i]['score'] = 0;
        $_SESSION['user'][$i]['timer'] = [];
    }

    header('Location: quiz.php');
    exit;
}

/* set up variables for current user */
// get the number of users/players
$numOfUsers = count($_SESSION['user']);
// set the current player (identified by array index of session['user']
if (!isset($_SESSION['userIdx'])){
    $_SESSION['userIdx'] = 0;
}
else {
    if ($_SESSION['userIdx'] >= $numOfUsers){
        $_SESSION['userIdx'] = 0;
    }
}
// set local var for current user
$currentUser = $_SESSION['user'][$_SESSION['userIdx']];
// get the index of the next player
$nextUserIdx = 0;
if ($numOfUsers > 1){
    $nextUserIdx = $_SESSION['userIdx'] + 1;
    // reset back to 0 since index counters are 0-based
    if ($nextUserIdx == $numOfUsers){
        $nextUserIdx = 0;
    }
}

// handle if the user submitted "next" after selecting an answer
if (isset($_GET['submit'])){
    // add number of seconds it took to answer to user's array
    if (count($_SESSION['user'][$_SESSION['userIdx']]['questionIDs']) != count($_SESSION['user'][$_SESSION['userIdx']]['timer'])){
        array_push($_SESSION['user'][$_SESSION['userIdx']]['timer'], $_POST['timer']);
    }
    // if they answered correct, increment the score counter
    if($_POST['correct'] == 1) {
        $_SESSION['user'][$_SESSION['userIdx']]['score']++;
    }
    else {
        array_push($_SESSION['user'][$_SESSION['userIdx']]['questionIncorrect'], $_POST['question_id']);
    }
    // always increment the number of questions answered counter
    $_SESSION['user'][$_SESSION['userIdx']]['questionCount']++;
    // increment the current user index
    $_SESSION['userIdx'] = $nextUserIdx;

    // if all users answered the QUIZ_MAX_QUESTIONS global
    if($nextUserIdx == 0 && $_SESSION['user'][$_SESSION['userIdx']]['questionCount'] == QUIZ_MAX_QUESTIONS) {
        header('Location: results.php');
        exit;
    }
    // else redirect to quiz (w/o ?submit=true in query string to prevent reload issues)
    else {
        header('Location: quiz.php');
        exit;
    }

}

/*
 * get question from DB
 * - do not repeat any question in a round
 * - do not get a new question for current user (e.g. if user presses refresh)
 */
$questionID = -1;
$currentQuestionNumber = count($currentUser['questionIDs']);
$currentRound = $currentQuestionNumber + 1;

// if # of questions for current user > # of Qs for next user, then the question was already loaded.  get the DB vals for current questionID
if ($currentQuestionNumber > $currentUser['questionCount']){
    $questionID = $currentUser['questionIDs'][$currentQuestionNumber - 1];
}
// otherwise, get a list of question IDs not to pull from the DB; include user's previous questions and any questions from other players in the current round
else {
    // add list of questions users already answered to the list of questions not to ask
    $_SESSION['questionIDs'] = $currentUser['questionIDs'];

    // for every user who has gone previously in the current round
    foreach ($_SESSION['user'] as $idx => $key){
        // get the last questions answered by players prior to current user for current round
        if ($idx < $_SESSION['userIdx']){
            $idList = $_SESSION['user'][$idx]['questionIDs'];
            if (count($idList) == $currentRound){
                array_push($_SESSION['questionIDs'], $idList[count($idList) -1]);
            }
        }
        // no need to check current user or any other players after
        else {
            break;
        }
    }

}

// get the question
include('includes/getQuestion.php');

require_once 'includes/template/header.php';
?>
<svg version="1.1" xmlns="http://www.w3.org/2000/svg" height="0" width="0">
    <defs>
        <filter id="blur" x="0" y="0">
            <feGaussianBlur stdDeviation="5" />
        </filter>
    </defs>
</svg>
<div class="container main-container" role="main">
    <div class="row equalizer">
        <div class="col-md-2">
            <div class="col-xs-6 col-md-12 score-title"><div class="score-title-text">Score</div></div>
<?php
foreach ($_SESSION['user'] as $idx => $key):
    $currentUserClass = ($_SESSION['userIdx'] == $idx) ? ' current-player' : '';
?>
            <div class="col-xs-6 col-md-12 score-box<?php echo $currentUserClass; ?>">
                <div class="user-score pull-left">
                    <?php echo $_SESSION['user'][$idx]['score'] ?>/<?php echo $_SESSION['user'][$idx]['questionCount']; ?>
                </div>
                <div class="user-number pull-left">
                    <?php echo $idx + 1; ?>
                </div>
            </div>
<?php
endforeach;
?>
        </div>

        <div class="col-md-6">
            <div class="well well-lg watch clearfix">
                <div class="clearfix">
                    <div class="question-bubble"><?php echo $currentUser['questionCount'] + 1; ?></div>
                    <div class="question-text"><?php echo $question; ?></div>
                </div>

                <div<?php echo ($questionType == 3) ? ' id="sort-list"' : ''; ?> class="answers clearfix list-group">
                    <?php if ($questionType == 1): ?>
                        <div class="answer clearfix">
                            <input type="radio" name="ans" value="0" id="ans0" class="styled">
                            <label for="ans0"><?php echo $answers[0]; ?></label>
                        </div>
                        <div class="answer clearfix">
                            <input type="radio" name="ans" value="1" id="ans1" class="styled">
                            <label for="ans1"><?php echo $answers[1]; ?></label>
                        </div>
                        <div class="answer clearfix">
                            <input type="radio" name="ans" value="2" id="ans2" class="styled">
                            <label for="ans2"><?php echo $answers[2]; ?></label>
                        </div>
                        <div class="answer clearfix">
                            <input type="radio" name="ans" value="3" id="ans3" class="styled">
                            <label for="ans3"><?php echo $answers[3]; ?></label>
                        </div>
                    <?php elseif ($questionType == 2): ?>
                        <div class="answer clearfix">
                            <input type="radio" name="ans" value="T" id="anst" class="styled">
                            <label for="anst">True</label>
                        </div>
                        <div class="answer clearfix">
                            <input type="radio" name="ans" value="F" id="ansf" class="styled">
                            <label for="ansf">False</label>
                        </div>
                    <?php elseif ($questionType == 3):
                        $currentAnswerOrder = [];
                        foreach($answersDB as $idx => $key):
                            array_push($currentAnswerOrder, $answersDB[$idx]["answer_id"]);
                    ?>
                        <div class="answer list-group-item clearfix">
                            <input type="hidden" name="ans" value="<?php echo $answersDB[$idx]["answer_id"]; ?>" id="ans<?php echo $idx; ?>">
                            <label for="ans<?php echo $idx; ?>"><span class="drag-handle">☰</span> <?php echo $answersDB[$idx]["answer"]; ?></label>
                        </div>
                    <?php
                        endforeach;
                    ?>
                        <div>
                            <input type="hidden" id="answer-order" name="answer-order" value="<?php echo implode(",", $currentAnswerOrder); ?>">
                        </div>
                    <?php
                    endif;
                    ?>
                </div>
                <button type="button" name="submit" id="submit" onClick="checkAnswer();" class="btn btn-default">Submit</button>
            </div>
        </div>

        <div class="col-md-4">
            <div class="well well-lg watch clearfix">
                <div id="correct-text" class="hidden">
                    <h3 class="first green">You're Right!</h3>
                    <span class="smaller-text"><?php echo $answerInfo; ?></span>
                    <br/><br/>
                    <form name="next-correct" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>?submit=true">
                        <input type="hidden" name="correct" id="correct" value="1">
                        <input type="hidden" name="question_id" value="<?php echo $questionID; ?>">
                        <input type="hidden" name="timer" value="" class="timer">
                        <button type="submit" name="next" id="next" class="btn btn-default">Next</button>
                    </form>
                </div>
                <div id="incorrect-text" class="hidden">
                    <h3 class="first red">Wrong!</h3>
                    <span class="smaller-text"><?php echo $answerInfo; ?></span>
                    <br/><br/>
                    <form name="next-correct" method="post" action="<?php echo htmlentities($_SERVER['PHP_SELF']); ?>?submit=true">
                        <input type="hidden" name="correct" id="correct" value="0">
                        <input type="hidden" name="question_id" value="<?php echo $questionID; ?>">
                        <input type="hidden" name="timer" value="" class="timer">
                        <button type="submit" name="next" id="next" class="btn btn-default">Next</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="round-notification" class="modal" tabindex="-1" role="dialog" aria-labelledby="basicModal" aria-hidden="true">
    <div class="modal-dialog" style="text-align:center;">
        <div class="modal-body">
            <h2><span id="round-username"><?php echo $currentUser['username']; ?></span> <img id="round-icon" src="img/round.png" width="98" height="103" alt=""> Round <span id="round-number"><?php echo $currentUser['questionCount'] + 1; ?></span></h2>
        </div>
    </div>
</div>
<?php
// obfuscate JS by minifying
$pageJs = <<<EOSCRIPT
    var timerStart = Date.now();
    var checkboxHeight = 50;
    var radioHeight = 34;
    var selectWidth = 190;

    /* No need to change anything after this */

    document.write('<style type="text/css">input.styled { display: none; } select.styled { position: relative; width: ' + selectWidth + 'px; opacity: 0; filter: alpha(opacity=0); z-index: 5; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>');

    var Custom = {
        init: function() {
            var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
            for(a = 0; a < inputs.length; a++) {
                if((inputs[a].type == "checkbox" || inputs[a].type == "radio") && inputs[a].className.indexOf("styled") > -1) {
                    span[a] = document.createElement("span");
                    span[a].className = inputs[a].type;

                    if(inputs[a].checked == true) {
                        if(inputs[a].type == "checkbox") {
                            position = "0 -" + (checkboxHeight*2) + "px";
                            span[a].style.backgroundPosition = position;
                        } else {
                            position = "0 -" + (radioHeight*2) + "px";
                            span[a].style.backgroundPosition = position;
                        }
                    }
                    inputs[a].parentNode.insertBefore(span[a], inputs[a]);
                    inputs[a].onchange = Custom.clear;
                    if(!inputs[a].getAttribute("disabled")) {
                        span[a].onmousedown = Custom.pushed;
                        span[a].onmouseup = Custom.check;
                    } else {
                        span[a].className = span[a].className += " disabled";
                    }
                }
            }
            inputs = document.getElementsByTagName("select");
            for(a = 0; a < inputs.length; a++) {
                if(inputs[a].className.indexOf("styled") > -1) {
                    option = inputs[a].getElementsByTagName("option");
                    active = option[0].childNodes[0].nodeValue;
                    textnode = document.createTextNode(active);
                    for(b = 0; b < option.length; b++) {
                        if(option[b].selected == true) {
                            textnode = document.createTextNode(option[b].childNodes[0].nodeValue);
                        }
                    }
                    span[a] = document.createElement("span");
                    span[a].className = "select";
                    span[a].id = "select" + inputs[a].name;
                    span[a].appendChild(textnode);
                    inputs[a].parentNode.insertBefore(span[a], inputs[a]);
                    if(!inputs[a].getAttribute("disabled")) {
                        inputs[a].onchange = Custom.choose;
                    } else {
                        inputs[a].previousSibling.className = inputs[a].previousSibling.className += " disabled";
                    }
                }
            }
            //document.onmouseup = Custom.clear;
            var sortEl = document.getElementById('sort-list');
            if (sortEl){
                var sortable = Sortable.create(sortEl, {
                    draggable: '.answer',
                    handle: ".drag-handle",
                    onSort: function(e){
                        var newOrder = [];
                        \$('.answer.list-group-item input').each(function(){
                            newOrder.push($(this).val() * 1);
                        });
                        \$('#answer-order').val(newOrder.join());
                        console.log(\$('#answer-order').val());
                    }
                });
            }
        },
        pushed: function() {
            element = this.nextSibling;
            if(element.checked == true && element.type == "checkbox") {
                this.style.backgroundPosition = "0 -" + checkboxHeight*3 + "px";
            } else if(element.checked == true && element.type == "radio") {
                this.style.backgroundPosition = "0 -" + radioHeight*3 + "px";
            } else if(element.checked != true && element.type == "checkbox") {
                this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
            } else {
                this.style.backgroundPosition = "0 -" + radioHeight + "px";
            }
        },
        check: function() {
            element = this.nextSibling;
            if(element.checked == true && element.type == "checkbox") {
                this.style.backgroundPosition = "0 0";
                element.checked = false;
            } else {
                if(element.type == "checkbox") {
                    this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
                } else {
                    this.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
                    group = this.nextSibling.name;
                    inputs = document.getElementsByTagName("input");
                    for(a = 0; a < inputs.length; a++) {
                        if(inputs[a].name == group && inputs[a] != this.nextSibling) {
                            inputs[a].previousSibling.style.backgroundPosition = "0 0";
                        }
                    }
                }
                element.checked = true;
            }
        },
        clear: function() {
            inputs = document.getElementsByTagName("input");
            for(var b = 0; b < inputs.length; b++) {
                if(inputs[b].type == "checkbox" && inputs[b].checked == true && inputs[b].className.indexOf("styled") > -1) {
                    inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
                } else if(inputs[b].type == "checkbox" && inputs[b].className.indexOf("styled") > -1) {
                    inputs[b].previousSibling.style.backgroundPosition = "0 0";
                } else if(inputs[b].type == "radio" && inputs[b].checked == true && inputs[b].className.indexOf("styled") > -1) {
                    inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
                } else if(inputs[b].type == "radio" && inputs[b].className.indexOf("styled") > -1) {
                    inputs[b].previousSibling.style.backgroundPosition = "0 0";
                }
            }
        },
        choose: function() {
            option = this.getElementsByTagName("option");
            for(d = 0; d < option.length; d++) {
                if(option[d].selected == true) {
                    document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue;
                }
            }
        }
    }
    window.onload = Custom.init;

    function checkAnswer(){
        \$('.timer').val( (Date.now() - timerStart) / 1000 );

        var correctAnswer = "$correctAnswer";
        var questionType = "$questionType";

        if (questionType == 1) {
            if (document.getElementById('ans0').checked) {
                var answer = document.getElementById('ans0').value;
            }
            else if (document.getElementById('ans1').checked) {
                var answer = document.getElementById('ans1').value;
            }
            else if (document.getElementById('ans2').checked) {
                var answer = document.getElementById('ans2').value;
            }
            else if (document.getElementById('ans3').checked) {
                var answer = document.getElementById('ans3').value;
            }
        } else if (questionType == 2) {
            if (document.getElementById('anst').checked) {
                var answer = document.getElementById('anst').value;
            }
            else if (document.getElementById('ansf').checked) {
                var answer = document.getElementById('ansf').value;
            }
        } else if (questionType == 3){
            var answer = document.getElementById('answer-order').value;
        }

        if (answer == correctAnswer) {
            var id="correct-text";
            var myClassName="hidden"; //must keep a space before class name
            var d;
            d=document.getElementById(id);
            d.className=d.className.replace(myClassName,"");
        }
        else {
            var id="incorrect-text";
            var myClassName="hidden"; //must keep a space before class name
            var d;
            d=document.getElementById(id);
            d.className=d.className.replace(myClassName,"");
        }
        enviroplay.setColumnHeights();

        var r1 = document.getElementById('submit');
        r1.setAttribute("hidden", "true");
    }

    /*\$('#myModal').reveal({
        animation: 'fade',
        animationspeed: 300,
        closeonbackgroundclick: true,
        dismissmodalclass: 'close-reveal-modal',
        onshow: function(){
            setTimeout(function(){ $('#myModal').trigger('reveal:close'); }, 1200);
        }
    });*/
    \$(document).ready(function(){
        \$('#round-notification').on('show.bs.modal', function(e){
        }).on('shown.bs.modal', function(e){
            $(this).find('.modal-dialog').css({
                'top': Math.max(0, (\$(window).height() / 2) - \$('#round-icon').attr("height") + \$(window).scrollTop()) + "px"
            });
            setTimeout(function(){
                \$('#round-notification').modal('hide');
            }, 1500);
        }).on('hidden.bs.modal', function(e){
            timerStart = Date.now();
        }).modal('show');
    });
EOSCRIPT;

// include minifier
include_once "includes/JShrink.php";

// minify JS and wrap in script tag
$pageJs = "<script type=\"text/javascript\" src=\"js/Sortable.min.js\"></script>\n<script type=\"text/javascript\">\n" . \JShrink\Minifier::minify($pageJs) . "\n</script>\n";

require_once 'includes/template/footer.php';
?>
