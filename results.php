<?php
session_start();
require_once 'includes/utilityFunctions.php';

// if user clicks the "all done" button, clear user's quiz session vars and redirect to index
if (isset($_GET['submit'])){
    for ($i = 0; $i < count($_SESSION['user']); $i++){
        $_SESSION['user'][$i]['questionIDs'] = [];
        $_SESSION['user'][$i]['questionIncorrect'] = [];
        $_SESSION['user'][$i]['questionCount'] = 0;
        $_SESSION['user'][$i]['score'] = 0;
        $_SESSION['user'][$i]['timer'] = [];
    }

    header('Location: index.php');
}

// else display summary
// determine who the "winner" is
$numUsers = count($_SESSION['user']);
$maxScore = 0;
$maxScoreUser = [];
for ($i = 0; $i < $numUsers; $i++){
    if ($_SESSION['user'][$i]['score'] > $maxScore){
        $maxScore = $_SESSION['user'][$i]['score'];
        $maxScoreUser = [$i];
    }
    else if ($_SESSION['user'][$i]['score'] == $maxScore){
        array_push($maxScoreUser, $i);
    }
}

$pageTitle = "Results";

require_once 'includes/template/header.php';
?>

<div class="container main-container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>
<?php
// display "winner" notification only if there's > 1 player
if (count($_SESSION['user']) > 1):
?>
    <div class="msg-container" style="margin-top:1em;">
        <div class="alert alert-success alert-dismissible" role="alert">
<?php
    // if there are > 1 winner, display tie message
    if (count($maxScoreUser) > 1):
?>
                <h2 class="first alert-heading">It's a Tie! The Winners are...</h2>
<?php
        // and then display each tied winner
        foreach ($maxScoreUser as $i):
?>
                <p style="text-align:center;"><?php echo $_SESSION['user'][$i]['username']; ?></p>
<?php
        endforeach;
?>

<?php
    // if there's only one winner
    else:
?>
                <h2 class="first alert-heading">And the Winner is...</h2>

                <p style="text-align:center;"><?php echo $_SESSION['user'][$maxScoreUser[0]]['username']; ?></p>
<?php
    endif;
?>
        </div>
    </div>

<?php
endif;
?>
    <div class="well well-lg clearfix">
        <div class="row clearfix">
<?php
$canvasSupportMsg = "Sorry, your browser doesn't support the &lt;canvas&gt; element.";
// display results summary for each player
for ($i = 0; $i < $numUsers; $i++):
    $currentUser = $_SESSION['user'][$i];
    $formatter = new NumberFormatter('en_US', NumberFormatter::PERCENT);

    // first display percentage donut chart
?>
            <div class="col-md-6<?php echo ($numUsers == 1) ? ' col-md-offset-3' : ''; ?> result-well">
                <h2 class="first"><?php echo $currentUser['fname'] . " (" . $currentUser['username'] . ")"; ?></h2>

<?php
    // if they answered questions
    if ($currentUser['questionCount'] > 0):
        $qQuestionsByTopic = DB::query('SELECT q.question_id, rt.topic_area_id, rt.topic_area FROM question q LEFT JOIN ref_topic_area rt ON q.topic_area_id = rt.topic_area_id WHERE q.question_id IN %li ORDER BY rt.topic_area', $currentUser['questionIDs']);
?>
                <p class="score">You got <?php echo $formatter->format($currentUser['score'] / $currentUser['questionCount']); ?> (<?php echo '<span id="player' . $i . '-score">' . $currentUser['score'] . '</span>/<span id="player' . $i . '-questions">' . $currentUser['questionCount'] . '</span>'; ?>) of your quiz questions correct.</p>

                <canvas id="player<?php echo $i; ?>-percent-chart" class="chartjs"><?php echo $canvasSupportMsg; ?></canvas>

                <div class="inner-donut"><span><?php echo $formatter->format($currentUser['score'] / $currentUser['questionCount']); ?></span></div>

                <ul class="topic sr-only">
<?php
        // loop over topics to: 1) generate summary; 2) display results of breakdown of number of questions per topic; 3) create data object for stacked bar chart (number of questions answered correctly vs. incorrectly)
        $topicBreakdown = [];
        $currentTopic = "";
        $currentTopicQuestionCnt = 0;
        $currentNumberIncorrect = 0;
        $maxTopicNumber = 0;
        $maxTopicKey = [];
        foreach($qQuestionsByTopic AS $j => $key):
            if ($qQuestionsByTopic[$j]['topic_area'] != $currentTopic):
                if ($currentTopic != ""):
                    echo $currentTopicQuestionCnt . "/" . $currentNumberIncorrect . "</li>";
                endif;
                $currentTopic = $qQuestionsByTopic[$j]['topic_area'];
                $topicBreakdown[$currentTopic] = [
                    "correct" => 0,
                    "incorrect" => 0
                ];
                echo "                    <li>\n" . $qQuestionsByTopic[$j]['topic_area'] . ':' ;
                $currentTopicQuestionCnt = 0;
                $currentNumberIncorrect = 0;
            endif;

            if (in_array($qQuestionsByTopic[$j]['question_id'], $currentUser['questionIncorrect'])){
                $topicBreakdown[$currentTopic]["incorrect"]++;
                $currentNumberIncorrect++;
            }
            else {
                $topicBreakdown[$currentTopic]["correct"]++;
            }

            $tmp = $topicBreakdown[$currentTopic]["correct"] + $topicBreakdown[$currentTopic]["incorrect"];

            if ($tmp > $maxTopicNumber):
                $maxTopicKey = [$currentTopic];
                $maxTopicNumber = $tmp;
            elseif ($tmp == $maxTopicNumber):
                array_push($maxTopicKey, $currentTopic);
            endif;

            $currentTopicQuestionCnt++;

        endforeach;
        echo $currentTopicQuestionCnt . "/" . $currentNumberIncorrect . "</li>";

        // determine which question was most difficult (used in summary)
        $maxIncorrectPercentage = 0;
        $maxIncorrectPercentageKey = [];
        foreach($topicBreakdown AS $j => $key):
            $tmp = $topicBreakdown[$j]["incorrect"] / ($topicBreakdown[$j]["correct"] + $topicBreakdown[$j]["incorrect"]);
            // if current topic has a higher percentage incorrect, store it
            if ($tmp > $maxIncorrectPercentage):
                $maxIncorrectPercentageKey = [$j];
                $maxIncorrectPercentage = $tmp;
            elseif ($tmp == $maxIncorrectPercentage):
                array_push($maxIncorrectPercentageKey, $j);
            endif;
        endforeach;
?>
                </ul>

                <p>You were asked the most questions in the topic of "<?php echo $maxTopicKey[0];?>", answering <?php echo $topicBreakdown[$maxTopicKey[0]]["correct"] . ' question' . (($topicBreakdown[$maxTopicKey[0]]["correct"] != 1) ? 's' : '') . " out of " . $maxTopicNumber; ?> correctly.  You also had the most difficulty in the "<?php echo $maxIncorrectPercentageKey[0]; ?>" topic, answering <?php echo $formatter->format(1 - $maxIncorrectPercentage); ?> of questions correctly.</p>

                <div style="margin:1em;">
                    <canvas id="player<?php echo $i; ?>-topic-chart" class="chartjs"><?php echo $canvasSupportMsg; ?></canvas>
                </div>

                <table class="q-timer sr-only">
                    <thead>
                        <tr>
                            <th id="q-num">Question No.</th>
                            <th id="q-sec">Seconds</th>
                        </tr>
                    </thead>
                    <tbody>
<?php
        $totalSeconds = 0;
        foreach($currentUser['timer'] AS $idx => $val):
            $totalSeconds += $val;
?>
                        <tr>
                            <td headers="q-num"><?php echo $idx + 1; ?></td>
                            <td class="q-sec-val" headers="q-sec"><?php echo round($val, 2); ?></td>
                        </tr>
<?php
        endforeach;
        $avgSeconds = $totalSeconds / count($currentUser['timer']);
?>
                    </tbody>
                </table>

                <p>You took an average of <span id="player<?php echo $i; ?>-timer-avg"><?php echo round($avgSeconds, 2); ?></span> seconds to answer each question:</p>

                <div style="margin:1em;">
                    <canvas id="player<?php echo $i; ?>-timer-chart" class="chartjs"><?php echo $canvasSupportMsg; ?></canvas>
                </div>
<?php
    // if they didn't answer any questions...
    else:
?>
                <p>You didn't answer any questions!</p>
<?php
    // end if they answered questions
    endif;
?>
            </div>
<?php
// end display results summary for each player
endfor;
?>
        </div>

        <p>
            <a href="<?php echo $_SERVER['PHP_SELF']; ?>?submit=true"><button class="btn btn-default pull-right">ALL DONE!</button></a>
        </p>
    </div>

</div>

<?php
$pageJs = <<<ENDOFSCRIPT
\$(document).ready(function(i){
    \$('.inner-donut').css({
        'margin-top': (\$('.chartjs').height() + \$('.inner-donut').height() + 30 )/2 * -1 + 'px',
        'margin-bottom': (\$('.chartjs').height() + \$('.inner-donut').height() )/2 + 'px',
        'margin-left': (\$('.chartjs').width() - \$('.inner-donut span').width() + 30)/2 + 'px'
    });
    var ctx = [],
        donutOptions = {
            maintainAspectRatio: true,
            percentageInnerCutout: 60,
            responsive: true

        },
        stackedBarOptions = {
            barStrokeWidth: 1,
            maintainAspectRatio: true,
            multiTooltipTemplate: "<%= value %> <%= datasetLabel %>",
            responsive: true,
            stacked: true
        },
        lineBarOptions = {
            bezierCurveTension: 0.25,
            maintainAspectRatio: true,
            multiTooltipTemplate: "<%= value %> <%= datasetLabel %>",
            pointDotRadius: 3,
            responsive: true,
            scaleLabel: "<%= value + '.0 sec' %>",
            scaleOverride: true,
            scaleStartValue: 0,
            scaleStepWidth: 1,
            scaleSteps: 30
            //bezierCurve: false
        };

    \$('.score').each(function(i){
        var score = document.getElementById('player' + i + '-score').innerHTML * 1,
            data = [
                {
                    value: score,
                    color: "#74ca1d",
                    highlight: "#bce791",
                    label: "Questions Answered Correctly"
                },
                {
                    value: (document.getElementById('player' + i + '-questions').innerHTML*1) - score,
                    color:"#f7464a",
                    highlight: "#ff5a5e",
                    label: "Questions Answered Incorrectly"
                }
            ];
        ctx.push(document.getElementById('player' + i + '-percent-chart').getContext("2d"));
        var ctxIdx = ctx.length - 1;
        var myDoughnutChart = new Chart(ctx[ctxIdx]).Doughnut(data, donutOptions);
    });

    \$('.topic').each(function(i){
        var userLabels = [],
            userCorrect = [],
            userIncorrect = [];

        \$(this).find('li').each(function(){
            var tmp = \$(this).html().split(":"),
                vals = \$.trim(tmp[1]).split("/");

            userLabels.push(tmp[0]);
            var tmpTotal = \$.trim(vals[0]) * 1;
            var tmpIncorrect = \$.trim(vals[1]) * 1;
            userCorrect.push(tmpTotal - tmpIncorrect);
            userIncorrect.push(tmpIncorrect);
        });

        var data = {
            labels: userLabels,
            datasets: [
                {
                    label: "incorrect",
                    fillColor: "rgba(247, 70, 74, 1)",
                    strokeColor: "rgba(255, 255, 255, 0.8)",
                    highlightFill: "rgba(255, 90, 94, 0.75)",
                    highlightStroke: "gba(255, 255, 255, 0.8)",
                    data: userIncorrect
                },
                {
                    label: "correct",
                    fillColor: "rgba(116, 202, 29, 1)",
                    strokeColor: "rgba(255, 255, 255, 0.8)",
                    highlightFill: "rgba(188, 231, 145, 0.75)",
                    highlightStroke: "gba(255, 255, 255, 0.8)",
                    data: userCorrect
                }
            ]
        };

        ctx.push(document.getElementById('player' + i + '-topic-chart').getContext("2d"));
        var ctxIdx = ctx.length - 1;
        var myStackedBarChart = new Chart(ctx[ctxIdx]).StackedBar(data, stackedBarOptions);

    });

    \$('.q-timer').each(function(i){
        var labels = [],
            data,
            dataset = [],
            avgDataset = [],
            avgSec = \$('#player' + i + '-timer-avg').html() * 1,
            \$trs = \$(this).find('tbody tr'),
            maxSecs = 0;

        \$trs.each(function(idx){
            labels.push('Question ' + (idx+1) );
            var \$tds = \$(this).children('td.q-sec-val');

            \$tds.each(function(){
                var roundSecs = \$(this).html() * 1;
                dataset.push(roundSecs);
                avgDataset.push(avgSec);
                if (roundSecs > maxSecs){
                    maxSecs = roundSecs;
                }
            });
        });

        lineBarOptions.scaleSteps = Math.ceil(maxSecs) + 1;

        data = {
            labels: labels,
            datasets: [
                {
                    label: "secs to completion",
                    fillColor: "rgba(116, 202, 29, 1)",
                    strokeColor: "rgba(255, 255, 255, 0.8)",
                    pointColor: "rgba(116, 202, 29, 1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#000",
                    pointHighlightStroke: "rgba(220, 220, 220, 1)",
                    data: dataset
                },
                {
                    label: "secs average",
                    fillColor: "rgba(220, 220, 220, 0.2)",
                    strokeColor: "rgba(220, 220, 220, 1)",
                    pointColor: "rgba(220, 220, 220 ,1)",
                    pointStrokeColor: "#fff",
                    pointHighlightFill: "#000",
                    pointHighlightStroke: "rgba(220, 220, 220, 1)",
                    data: avgDataset
                }
            ]
        }

        ctx.push(document.getElementById('player' + i + '-timer-chart').getContext("2d"));
        var ctxIdx = ctx.length - 1;
        var myLineChart = new Chart(ctx[ctxIdx]).Line(data, lineBarOptions);;
    });

});
ENDOFSCRIPT;

// include minifier
include_once "includes/JShrink.php";

// minify JS and wrap in script tag
$pageJs = "<script type=\"text/javascript\" src=\"js/Chart.min.js\"></script>\n<script type=\"text/javascript\" src=\"js/Chart.StackedBar.min.js\"></script>\n<script type=\"text/javascript\">\n" . \JShrink\Minifier::minify($pageJs) . "\n</script>\n";

require_once 'includes/template/footer.php';
?>
