<?php
require_once 'includes/utilityFunctions.php';

$pageTitle = "Player Start";

if (!isset($_GET['idx'])){
    $_GET['idx'] = 0;
}
if (isset($_POST['gradelevelid'])){
    // if user changed grade levels, reset the selected topics
    if (!isset($_SESSION['gradelevel']) || $_POST['gradelevelid'] != $_SESSION['gradelevel']){
        $_SESSION['topics'] = [];
    }
    $_SESSION['gradelevel'] = $_POST['gradelevelid'];
}
if (!$isUserLoggedIn){
    $_SESSION['user'] = [];
}

require_once 'includes/template/header.php';
?>

<div class="container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>

    <div class="row clearfix equalizer">
<?php
$numPlayers = count($_SESSION['user']);
if ($numPlayers > 0 || (isset($_SESSION['gradelevel']) && is_int($_SESSION['gradelevel'])) || (isset($_SESSION['topics']) && count($_SESSION['topics']) > 0)):
?>
        <div class="col-md-4">
            <div class="well well-lg watch">
<?php
    if ($numPlayers > 0):
?>
                <h2 class="first">Signed-in Player<?php echo ($numPlayers != 1) ? 's' : ''; ?>:</h2>

                <ul>
<?php
        foreach($_SESSION['user'] as $idx => $key):
?>
                    <li><?php echo $_SESSION['user'][$idx]['fname'] . ' (' . $_SESSION['user'][$idx]['username'] . ')'; ?></li>
<?php
        endforeach;
?>
                </ul>

<?php
    endif;

    if (isset($_SESSION['gradelevel'])):
        $qLevel = DB::queryFirstRow('SELECT * FROM ref_level WHERE level_id = %i', $_SESSION['gradelevel']);
?>
                <h2>Game Level:</h2>
                <ul>
                    <li><?php echo $qLevel['level']; ?></li>
                </ul>
<?php
    endif;

    if (isset($_SESSION['topics']) && count($_SESSION['topics'])):
        $qTopic = DB::query('SELECT * FROM ref_topic_area WHERE topic_area_id IN %li', $_SESSION['topics']);
?>
                <h2>Topics:</h2>
                <ul>
<?php
        foreach($qTopic AS $topic):
?>
                    <li><?php echo $topic['topic_area']; ?></li>
<?php
        endforeach;
?>
                </ul>
<?php
    endif;
?>
            </div>
        </div>
<?php
endif;

if ($numPlayers < QUIZ_MAX_PLAYERS && $numPlayers != $_GET['idx'] + 1):
    $nextPlayer = ' #' . ($numPlayers + 1);
    if ($numPlayers > 0):
?>
        <div class="col-md-8">
<?php
    else:
?>
        <div class="col-md-6 col-md-offset-3">
<?php
    endif;
?>
            <div class="well well-lg watch">
                <h2 class="first">Sign in Player <?php echo $nextPlayer; ?>:</h2>

                <p>Prior to start, all players must be signed in.</p>

                <form name="signin" method="post" action="includes/login.php">
                    <div class="form-group">
                        <label for="myusername" class="sr-only">Username</label>
                        <input type="text" id="myusername" name="myusername" placeholder="Username" class="form-control">
                    </div>

                    <div class="form-group">
                        <label for="mypassword" class="sr-only">Password</label>
                        <input type="password" id="mypassword" name="mypassword" placeholder="Password" class="form-control">
                        <small><a href="forgotPassword.php">forgot username/password</a></small>
                    </div>

                    <p class="last">
                        <button type="submit" class="btn btn-default">Sign in</button>
                        <a href="register.php"><button type="button" class="btn btn-lg btn-link">Register</button></a>
                    </p>
                </form>
            </div>
        </div>
<?php
else:
    // get all grade levels' questions & topics
    $qTopics = DB::query('SELECT rl.*, rt.* FROM ref_level rl LEFT OUTER JOIN question q ON rl.level_id = q.level_id LEFT OUTER JOIN ref_topic_area rt ON q.topic_area_id = rt.topic_area_id ORDER BY rl.level_id DESC, rt.topic_area');
?>
        <div class="col-md-8">
<?php
    //if (!isset($_SESSION['gradelevel'])):
?>
            <div class="col-md-6" style="text-align:center;">
                <div class="well well-lg watch<?php echo (!isset($_SESSION['gradelevel']) || $_SESSION['gradelevel'] == "") ? ' inner-well' : ''; ?>">
                    <h2 class="first" style="margin-bottom:1em;">Select Game Level:</h2>
<?php
    $currentGrade = "";
    foreach($qTopics as $topic):
        if ($currentGrade != $topic['level_short_name']):
            $currentGrade = $topic['level_short_name'];
?>
<?php
            if (isset($_SESSION['gradelevel']) && $_SESSION['gradelevel'] == $topic['level_id']):
?>
                    <p><button type="button" class="btn btn-lg btn-default active"><?php echo $currentGrade; ?></button></p>
<?php
            else:
?>
                    <form method="post" action="<?php echo htmlentities($_SERVER['REQUEST_URI']); ?>">
                        <p>
                            <button type="submit" class="btn btn-lg btn-default"><?php echo $currentGrade; ?></button>
                            <input type="hidden" name="gradelevelid" id="gradelevelid" value="<?php echo $topic['level_id']; ?>">
                        </p>
                    </form>
<?php
            endif;

        endif;

    endforeach;
?>
                </div>
            </div>

            <div class="col-md-6" style="text-align:center;">
                <div class="well well-lg watch<?php echo (isset($_SESSION['gradelevel']) && !empty($_SESSION['gradelevel'])) ? ' inner-well' : ''; ?>">
                    <h2 class="first" style="margin-bottom:1em;">Select Topics:</h2>

<?php
    if (!isset($_SESSION['gradelevel']) || !is_numeric($_SESSION['gradelevel'])):
?>
                    <p>Please select a Game Level first.</p>
<?php
    else:
?>
                    <div class="row">
<?php
        $currentTopic = "";
        $cnt = 0;
        foreach($qTopics as $topic):
            if ($cnt == 2):
                $cnt = 0;
?>
                    </div>
                    <div class="row" style="margin-top:1em;">
<?php
            endif;
            if ($_SESSION['gradelevel'] == $topic['level_id']):
                if ($currentTopic != $topic['topic_area']):
                    $currentTopic = $topic['topic_area'];

?>
                        <div class="col-md-6">
                            <button id="topic-id-<?php echo $topic['topic_area_id']; ?>" type="button" class="btn btn-lg btn-default btn-topic<?php echo (in_array($topic['topic_area_id'], $_SESSION['topics']) || empty($_SESSION['topics'])) ? ' active' : ''; ?>"><?php echo $currentTopic; ?></button>
                        </div>
<?php
                    $cnt++;
                endif;
            endif;
        endforeach;
?>
                    </div>
                    <form method="post" action="quiz.php?reset=true" class="clearfix">
                        <button type="submit" class="btn btn-success pull-right">Start!</button>
                        <input type="hidden" name="topic" id="topic" value="<?php echo (isset($_SESSION['topics']) && is_array($_SESSION['topics'])) ? implode(",", $_SESSION['topics']) : ''; ?>">
                    </form>

<?php
    endif;
?>
                </div>
            </div>
        </div>
<?php
endif;
?>
    </div>
</div>
<?php
$pageJs = <<< ENDOFSCRIPT
\$(document).ready(function(e){
    \$('.btn-topic').click(function(e){
        var \$this = \$(this),
            topic_id = this.id.replace(/\D/g, ''),
            \$topic = \$('#topic'),
            topicArray = \$.trim(\$topic.val()).split(",").filter(function(n){ return n != "" }),
            idx = \$.inArray(topic_id + "", topicArray);

        if (\$this.hasClass('active')){
            if (idx != -1){
                topicArray.splice(idx);
            }
            \$this.removeClass('active');
        }
        else {
            if (idx == -1){
                topicArray.push(topic_id);
            }
            \$this.addClass('active');
        }
        if (topicArray.length){
            \$topic.val(topicArray.toString());
        }
        else {
            \$topic.val("");
        }
    });
});
ENDOFSCRIPT;

// include minifier
include_once "includes/JShrink.php";

// minify JS and wrap in script tag
$pageJs = "<script type=\"text/javascript\">\n" . \JShrink\Minifier::minify($pageJs) . "\n</script>\n";

require_once 'includes/template/footer.php';
?>
