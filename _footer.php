<?php

	if(isset($_SESSION['loggedin'])) {
		echo '<div class="username-box"><div class="username-text">' .$_SESSION["username"] . '</div></div>';

		$host = $_SERVER['REQUEST_URI'];

		if($host == '/enviroplay/quiz.php')
		{
			echo '
				<div class="footer-quit">
					<form method="post" action="results.php">
						<input type="submit" value="Quit Quiz">
					</form>
				</div>';
		}
		elseif($host == '/enviroplay/_main.php')
		{
			echo '
				<div class="footer-quit">
					<form method="post" action="includes/logout.php">
						<input type="submit" value="Logout">
					</form>
				</div>';
			if ($_SESSION['admin'] == 1) {
				echo '
				<div class="footer-quit">
					<form method="post" action="admin/admin.php">
						<input type="submit" value="Admin">
					</form>
				</div>';
			}
		}
		elseif($host == '/enviroplay/admin/admin.php')
		{
			echo '
				<div class="footer-quit">
					<form method="post" action="../includes/logout.php">
						<input type="submit" value="Logout">
					</form>
				</div>
				<div class="footer-quit">
					<form method="post" action="../_main.php">
						<input type="submit" value="Home">
					</form>
				</div>';
		}

	}

?>