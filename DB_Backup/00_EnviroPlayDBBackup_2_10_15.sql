CREATE DATABASE  IF NOT EXISTS `e3db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `e3db`;
-- MySQL dump 10.13  Distrib 5.6.17, for Win64 (x86_64)
--
-- Host: localhost    Database: e3db
-- ------------------------------------------------------
-- Server version	5.6.21-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answer`
--

DROP TABLE IF EXISTS `answer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `answer` (
  `answer_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `correct` varchar(1) NOT NULL,
  PRIMARY KEY (`answer_id`),
  KEY `fk_question_id_idx` (`question_id`),
  CONSTRAINT `fk_question_id` FOREIGN KEY (`question_id`) REFERENCES `question` (`question_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answer`
--

LOCK TABLES `answer` WRITE;
/*!40000 ALTER TABLE `answer` DISABLE KEYS */;
INSERT INTO `answer` VALUES (1,1,'Air Cleaners','N'),(2,2,'Drought','N'),(3,3,'Hot temperatures','N'),(4,4,'Ground Level Ozone','N'),(5,5,'Sulfur Dioxide and Nitrogen Oxides ','Y'),(6,6,'Lung Damage','N'),(7,7,'Sewage treatment plants, septic systems, development, and residential and commercial lawn fertilizers','N'),(8,8,'Mesosphere','N'),(9,9,' Increases','N'),(10,10,' The scientist captures an air sample in a beaker from the air outside his/her laboratory.','N'),(11,11,'Radon','N'),(12,12,'Dust mites','N'),(16,1,'Source control','Y'),(17,2,'Intense storms','N'),(18,3,'Wind','N'),(19,4,'Nitrogen','Y'),(20,5,'Water and Oxygen','N'),(21,6,'Asthma','N'),(22,7,'Septic systems, air pollution, agriculture, development, and runoff from roadways ','N'),(23,8,'Exosphere','N'),(24,9,' Decreases ','Y'),(25,10,'The scientist sends a weather balloon with a camera to take pictures at different altitudes.','N'),(26,11,'Environmental Tobacco Smoke','N'),(27,12,'Lead','Y'),(31,1,'Ventilation improvements','N'),(32,2,'Rainbows','Y'),(33,3,'Hail','Y'),(34,4,'Particulate Matter','N'),(35,5,'Carbon Dioxide','N'),(36,6,'Respiratory Illness','N'),(37,7,'Air pollution (from vehicle exhaust), sewage treatment plants, large-scale animal operations, agriculture, and industrial sources ','Y'),(38,8,'Metasphere ','Y'),(39,9,'Stays equal ','N'),(40,10,'The scientist sends a weather balloon to collect air samples at different altitudes.','Y'),(41,11,'Lead','N'),(42,12,' Mold','N'),(46,1,'Air fresheners','N'),(47,2,' Flooding ','N'),(48,3,'Sunlight','N'),(49,4,'Carbon Monoxide ','N'),(50,5,'Ozone and Particulate Matter','N'),(51,6,'All of the above. ','Y'),(52,7,'Development, septic systems, runoff from roadways, and residential and commercial lawn fertilizers','N'),(53,8,'Thermosphere','N'),(54,9,'Can go up or down, depending on what day of the week it is','N'),(55,10,'The scientist stares into the sun at different times of day to try and visually detect shifts in color.','N'),(56,11,' All of the Above','Y'),(57,12,'Nitrogen dioxide','N');
/*!40000 ALTER TABLE `answer` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `question` (
  `question_id` int(11) NOT NULL AUTO_INCREMENT,
  `level_id` int(11) NOT NULL,
  `topic_area_id` int(11) NOT NULL,
  `question_type_id` int(11) NOT NULL,
  `question` varchar(1000) NOT NULL,
  `right_wrong_desc` varchar(1000) DEFAULT NULL,
  `source` varchar(1000) DEFAULT NULL,
  `reminder` varchar(1000) DEFAULT NULL,
  `tf_ans` varchar(2) DEFAULT NULL,
  PRIMARY KEY (`question_id`),
  KEY `fk_level_id_idx` (`level_id`),
  KEY `fk_question_type_id_idx` (`question_type_id`),
  KEY `fk_topic_area_id_idx` (`topic_area_id`),
  CONSTRAINT `fk_level_id` FOREIGN KEY (`level_id`) REFERENCES `ref_level` (`level_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_question_type_id` FOREIGN KEY (`question_type_id`) REFERENCES `ref_question_type` (`question_type_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_topic_area_id` FOREIGN KEY (`topic_area_id`) REFERENCES `ref_topic_area` (`topic_area_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `question`
--

LOCK TABLES `question` WRITE;
/*!40000 ALTER TABLE `question` DISABLE KEYS */;
INSERT INTO `question` VALUES (1,1,1,1,'Which is the most effective solution to reduce most indoor air quality problems in the home? \r\n\r\n\r\n\r\n\r\n','Usually the most effective way to improve indoor air quality is to eliminate individual sources of pollution or to reduce their emissions. ','U.S. Environmental Protection Agency, Indoor Air, Publications and Resources, The Inside Story: A Guide to Indoor Air Quality\rwww.epa.gov/iaq/pubs/insidest.html#Improve5\r','What are some additional ways that you can improve the indoor air quality within your home? ',NULL),(2,1,1,1,'Which of the following weather events is not related to climate change?\r\n\r\n\r\n\r\n\r\n','Rainbows are caused by the splitting of white sunlight into it component colors by raindrops; this is an ordinary occurrence.','U.S. Environmental Protection Agency, Climate Change ? Health and Environmental Effects, Extreme Events\rwww.epa.gov/climatechange/effects/extreme.html\r','How does climate change effect droughts, intense storms and flooding? ',NULL),(3,1,1,1,'Which of the following has the least impact on air quality?\r\r\r\r','One way weather can affect air quality is through high temperature and bright sunlight. Ground-level ozone forms in the air from other pollutants including volatile organic compounds (VOCs) and nitrogen oxides (NOx). The higher the temperature and the more direct the sunlight, the more ozone is produced; this is why summertime is the more common time for unhealthy air quality. ','United States Department of Commerce: National Oceanic and Atmospheric Administration, Air Quality\rwww.noaawatch.gov/themes/air_quality.php\r','What things can you do in the summertime to reduce air pollution levels? ',NULL),(4,1,1,1,'Which is not a common air pollutant?\r\r\r\r','Clean air is a mixture of about 78 percent nitrogen. Nitrogen is not a pollutant; it dilutes the other gases in the atmosphere and is used by living things to make proteins.\r\r','NOAA National Weather Service JetStream ? Online School for Weather, The Atmosphere\rwww.srh.noaa.gov/jetstream//atmos/atmos_intro.htm\rU.S. Environmental Protection Agency: Six Common Air Pollutants, What Are the Six Common Air Pollutants?\rwww.epa.gov/air/urbanair\r','What can you do to minimize your output of air pollutants?',NULL),(5,1,1,1,'What are the primary contributors to acid rain?\r\r\r\r\r','Atmospheric pollutants, particularly oxides of sulfur and nitrogen, can cause precipitation to become more acidic when converted to sulfuric and nitric acids, hence the term ?acid rain.?','USGS: What is Acid Rain?\rpubs.usgs.gov/gip/acidrain/2.html\rU.S. Environmental Protection Agency; Acid Rain Student Site, pH Scale\rwww.epa.gov/acidrain/education/site_students/phscale.html#chart \r','Why is acid rain worse in the Northeast United States compared to other parts of the country? ',NULL),(6,1,1,1,'What are some harmful health effects associated with air pollution?\r\r\r\r','Air pollution is made up of many kinds of gases, droplets and particles that reduce the quality of the air. Air pollution includes ?smog? which forms near the ground from chemicals in the air.','Health Effects of Air Pollution: How Can Air Pollution Hurt My Health?\rwww.lbl.gov/Education/ELSI/Frames/pollution-health-effects-f.html\r','How does air pollution affect your lungs? ',NULL),(7,1,1,1,'The majority of excess nitrogen delivered to the Chesapeake Bay comes from which group of sources?\r\r\r\r\r','Excess nitrogen contributes to the Chesapeake Bay?s poor water quality. While nitrogen is needed for plant growth, human activities contribute more nitrogen than the Bay?s waters can handle.  \r','Chesapeake Bay Program Science, Restoration, and Partnership: Nutrients\rwww.chesapeakebay.net/nutrients.aspx?menuitem=14690\rChesapeake Bay Foundation, Water Quality Issues, Nitrogen and Phosphorus Pollution\rhttp://www.cbf.org/page.aspx?pid=913\rThe Chesapeake BAY TMDL and Virginia?s Phase 1 Watershed Implementation Plan WEBINAR\rwww.chesapeakebay.net/status_nitrogensources.aspx?menuitem=19797\r','How do farms contribute wastes to the Chesapeake Bay? ',NULL),(8,1,1,1,'Which is not a layer of the atmosphere?\r\n\r\n\r\n\r\n\r\n','The atmosphere consists of five layers. In order of highest to lowest altitude they are the: exosphere, thermosphere, mesosphere, stratosphere and troposphere. \r\n','NOAA: National Weather Service, JetStream ? Online School for Weather, Layers of the Atmosphere\rwww.srh.noaa.gov/jetstream//atmos/layers.htm\r','What is the benefit to having multiple layers within our atmosphere? ',NULL),(9,1,1,1,'Air density does what as altitude increases?\r\r\r\r\r','Atmospheric pressure and air density decrease as you go up, because there is less air above you in the atmosphere and therefore less weight pushing down on you. A lower density means oxygen and other gases are not pushed together as tightly. As a result, at 10,000 feet there are 30 percent less oxygen molecules in a given volume of air, relative to sea level. This is why you can get winded or even get ?altitude sickness? at higher elevations.\r','NOAA: National Weather Service, JetStream ? Online School for Weather, Air Pressure\rwww.srh.noaa.gov/jetstream//atmos/pressure.htm\r','',NULL),(10,1,1,1,'When studying the atmosphere, which of the following methods would \rmost likely yield the best information about the chemical compositions of \rits different layers?\r\r\r\r\r','','About.com Chemistry: What is the Chemical Compostion of Air?\rhttp://chemistry.about.com/od/chemistryfaqs/f/aircomposition.htm\rStratospheric Ozone, Monitoring and Research NOAA: Science: ozone Basics\rwww.ozonelayer.noaa.gov/science/basics.htm\r','Why is it important to know about the chemical composition of the atmosphere? ',NULL),(11,1,1,1,'Which of the following is an Indoor Air Quality (IAQ) pollutant?\r\r\r','The three major ways to reduce indoor air pollutants are: (1) source control, (2) ventilation improvements, and (3) air control (e.g., filters) \r','U.S. Environmental Protection Agency, Indoor Air, Publications and Resources, The Inside Story: A Guide to Air Quality\rwww.epa.gov/iaq/pubs/insidestory.html\rU.S. Environmental Protection Agency, Healthy Schools, Lessons for a Clean Educational Environment\repa.gov/highschool/air.htm\r','How can we reduce our risks of IAQ pollutants? ',NULL),(12,1,1,1,'Which of the following is not an indoor environmental asthma trigger?\r\r\r\r','While exposure to lead can lead to numerous adverse health effects, it does not trigger asthma attacks. \r','U.S. Environmental Protection Agency, Asthma, Asthma Triggers: Gain Control\rwww.epa.gov/asthma/triggers.html\r','How many of the above indoor air pollutants would you find in your own home? \rHave you ever checked for any of these?\rHow would you check for these? \r',NULL),(16,1,1,2,'Mitigation is the human intervention to reduce the sources or enhance the sinks of greenhouse gases.','','IPCC, 2014: Summary for Policymakers, In: Climate Change 2014, Mitigation of Climate Change. Contribution of\rWorking Group III to the Fifth Assessment Report of the Intergovernmental Panel on Climate Change [Edenhofer,\rO., R. Pichs-Madruga, Y. Sokona, E. Farahani, S. Kadner, K. Seyboth, A. Adler, I. Baum, S. Brunner, P. Eickemeier, B.\rKriemann, J. Savolainen, S. Schl_mer, C. von Stechow, T. Zwickel and J.C. Minx (eds.)]. Cambridge University Press,\rCambridge, United Kingdom and New York, NY, USA.','\r','T'),(17,1,1,2,'Climate change can impact human health, food security, biodiversity, local environmental quality, energy access, livelihoods, and equitable\rsustainable development','','','\r','T'),(18,1,1,2,'An \"increase\" or \"take over\" is when a species enters an new ecosystem and has no predators.','An invasive species is a species occurring as a result of human activities beyond its accepted normal distribution and which threatens valued environmental, agricultural and personal resources by the damage it causes.','Australian Bureau of Statistics 1301.0- Year Book Australia, 2003 www.abs.gov.au/ausstats/abs@.nsf/46d1bc47ac9d0c7bca256c470025ff87/01cee6181b8b870cca256cae001599c2!OpenDocument ','\r','T'),(19,1,1,2,'Green house gases can potentially acumulate and mix globally over time?','','IPCC, 2014: Summary for Policymakers, In: Climate Change 2014, Mitigation of Climate Change. Contribution of\rWorking Group III to the Fifth Assessment Report of the Intergovernmental Panel on Climate Change [Edenhofer,\rO., R. Pichs-Madruga, Y. Sokona, E. Farahani, S. Kadner, K. Seyboth, A. Adler, I. Baum, S. Brunner, P. Eickemeier, B.\rKriemann, J. Savolainen, S. Schl_mer, C. von Stechow, T. Zwickel and J.C. Minx (eds.)]. Cambridge University Press,\rCambridge, United Kingdom and New York, NY, USA.','\r','T'),(20,1,1,2,'A water shed is an area of land that drains water, sediment and dissolved materials to a common receiving body or outlet.','The James River watershed is the largest river watershed in Virginia, covering approximately ? of the state. The James River, along with the Potomac/ Shenandoah, Rappahanock and York watersheds all feed the larger Chesapeake Bay watershed.','U.S. Environmental Protection Agency: Watershed Academy Web, Watershed Ecology\rwww.epa.gov/watertrain/ecology/ecology2.html  \rU.S. Environmental Protection Agency: Water, Watersheds-What is a Watershed?\rhttp://water.epa.gov/type/watersheds/whatis.cfm \rVirginia Department of Conservation and Recreation\rhttp://www.dcr.virginia.gov/soil_and_water/wsheds.shtml \rU.S. Environmental Protection Agency: Water, Healthy Watersheds http://www.epa.gov/nps/healthywatersheds/ \r','\r','T'),(21,1,1,2,'A wetland is saturated land, like a marsh, where water is the determining factor of flora and fauna communities.','Wetlands are areas where water covers the soil or is present either at or near the surface of the soil all year or for varying periods of time during the year, including during the growing season.\r','U.S. Environmental Protection Agency: Water, Wetlands _ Wetlands Definitions\rwww.epa.gov/owow/wetlands/what/definitions.html  \rU.S. Environmental Protection Agency: Water, Wetlands-What are Wetlands?\rwww.epa.gov/owow/wetlands/vital/what.html   \rU.S. Environmental Protection Agency: Wetlands Overview, What is a Wetland?\rhttp://water.epa.gov/type/wetlands/outreach/upload/overview.pdf \r','\r','T'),(22,1,1,2,'Permeability is the measure of the ease with which a liquid (or gas) can flow through soil.','Permeability measures how easily a liquid (or gas) can flow through the soil while _porosity_ measures how much water soil can hold.\r','U.S. Department of Agriculture, Natural Resources Conservation Service, Permeability Handout\rhttp://www.mt.nrcs.usda.gov/about/lessons/Lessons_Soil/permeability.html \r','\r','T'),(23,1,1,2,'An estuary is an area where fresh and salt water mix, producing variations in salinity and high biological activity.','The Chesapeake Bay is largest estuary in the world; some other familiar examples of U.S. estuaries include San Francisco Bay, Puget Sound, Boston Harbor, and Tampa Bay.','NOAA Estuary Education: National Estuarine Research Reserve System\rhttp://www.estuaries.gov \rThe Wild Classroom World Biomes: Biomes of the World, The Wild Classroom & Explore Biodiversity, Estuaries\rhttp://www.thewildclassroom.com/biomes/estuaries.html \r','\r','T'),(24,1,1,2,'A vernal pool is a small, isolated wetlands that retain water on a seasonal basis','Vernal pools collect water during winter and spring rains, changing in volume in response to varying weather patterns. Climatic changes associated with each season cause dramatic changes in the appearance of vernal pools. During a single season, pools may fill and dry several times.\r','The Vernal Pool: Information About Vernal Pools\rhttp://www.vernalpool.org/vpinfo_1.htm \rVirginia Department of Environmental Equality: Wetlands\rhttp://www.deq.virginia.gov/wetlands/wetlands.html \rFairfax County Virginia: Vernal Pools _ an Overlooked Natural Resource\rhttp://www.fairfaxcounty.gov/nvswcd/newsletter/vernalpools.htm\r','\r','T'),(25,1,1,2,'A dam is a barrier controlling flow of water: a barrier of concrete or earth that is built across a river or stream to obstruct or control the flow of wate','There are over 79,000 large dams in the United States.\r','Northwest  Power and Conservation Council: Energy, Guide to Major Dams, Grand Coulee Dam\rhttp://www.nwcouncil.org/energy/powersupply/dams/dam.asp?id=11 \rU.S. Army Corps of Engineers: National Inventory of Dams\rhttp://www.agc.army.mil/fact_sheet/nid.pdf \rU.S. Environmental Protection Agency: Polluted Runoff (Nonpoint Source Pollution) Management Measure for Erosion and Sediment Control - III. Dams Management Measures\rhttp://www.epa.gov/owow/NPS/MMGI/Chapter6/ch6-3a.html \r','\r','T'),(26,1,1,2,'A Hurricane is a natural disaster that forms over land masses.','Hurricanes cause environmental and property damage through their storm surges, high winds, torrential rains, tornados, mudslides and coastal erosion. \r','National Geographic: Hurricanes, Engine of Destruction\rhttp://environment.nationalgeographic.com/environment/natural-disasters/hurricane-profile/ \rNational Geographic, National Geographic News: Is Global Warming Hurricanes Worse?\rhttp://news.nationalgeographic.com/news/2005/08/0804_050804_hurricanewarming.html \r','\r','F'),(27,1,1,2,'The water cycle deals with the natural circulation of water on planet Earth','The water cycle is the circuit of water movement from the atmosphere to the Earth and its return to the atmosphere through various stages or processes, such as precipitation, interception, runoff, infiltration, storage, evaporation and transpiration. ','World Water Council\rhttp://www.worldwatercouncil.org/uploads/RTEmagicC_Water_cycle_01.JPG.jpg \rVirginia Department of Environmental Quality: Glossary of TMDL Terms\rhttp://www.deq.virginia.gov/tmdl/glossary.html#h \rUSGS Science for a Changing World: Water Science for Schools, Summary of the Water Cycle\rhttp://ga.water.usgs.gov/edu/watercyclesummary.html \rUSGS Science for a Changing World: Water Science for Schools, Where is Earth_s Water Located?\rhttp://ga.water.usgs.gov/edu/earthwherewater.html \r','\r','T'),(28,1,1,2,'Water and wind do not contribute to soil erisoon?','Construction projects, farming and deforestation are major causes of erosion caused by humans..\r','U.S. Environmental Protection Agency: Model Ordinances to Protect Local Resources _ Erosion and Sediment Control\rhttp://www.epa.gov/owow/nps/ordinance/erosion.htm \rInternational Erosion Control Association: Impact of Erosion Photos\rhttp://www.ieca.org/ImagesPhotogallery/photo_todd.jpg \rNASA GODDARD SPACE FLIGHT CENTER WALLOPS FLIGHT FACILITY STORMWATER POLLUTION PREVENTION PROGRAM FACT SHEET: Erosion and Sediment Control\rhttp://sites.wff.nasa.gov/code250/docs/soil_erosion.pdf \r','\r','F'),(29,1,1,2,'Sedimentation is the deposition of particles from a fluid such as water','Large-scale development means that a lot of land clearing and grading occurs. If the area has sloping land, soils that erode easily, and receives frequent periods of heavy rainfall, then water quality can be affected, usually in a negative way.\r\n','USGS Science for the Changing World, Water Science for Schools: The effect of Urbanization on water Quality: Erosion and Sedimentation\rhttp://ga.water.usgs.gov/edu/urbansed.html \r','\r','T');
/*!40000 ALTER TABLE `question` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_level`
--

DROP TABLE IF EXISTS `ref_level`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_level` (
  `level_id` int(11) NOT NULL AUTO_INCREMENT,
  `level` varchar(45) NOT NULL,
  PRIMARY KEY (`level_id`),
  UNIQUE KEY `level_UNIQUE` (`level`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_level`
--

LOCK TABLES `ref_level` WRITE;
/*!40000 ALTER TABLE `ref_level` DISABLE KEYS */;
INSERT INTO `ref_level` VALUES (1,'High School'),(4,'Lower Elementary'),(2,'Middle School'),(3,'Upper Elementary');
/*!40000 ALTER TABLE `ref_level` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_question_type`
--

DROP TABLE IF EXISTS `ref_question_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_question_type` (
  `question_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_type` varchar(45) NOT NULL,
  PRIMARY KEY (`question_type_id`),
  UNIQUE KEY `question_type_UNIQUE` (`question_type`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_question_type`
--

LOCK TABLES `ref_question_type` WRITE;
/*!40000 ALTER TABLE `ref_question_type` DISABLE KEYS */;
INSERT INTO `ref_question_type` VALUES (1,'Multiple Choice'),(2,'True/False');
/*!40000 ALTER TABLE `ref_question_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ref_topic_area`
--

DROP TABLE IF EXISTS `ref_topic_area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ref_topic_area` (
  `topic_area_id` int(11) NOT NULL AUTO_INCREMENT,
  `topic_area` varchar(45) NOT NULL,
  PRIMARY KEY (`topic_area_id`),
  UNIQUE KEY `topic_area_UNIQUE` (`topic_area`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ref_topic_area`
--

LOCK TABLES `ref_topic_area` WRITE;
/*!40000 ALTER TABLE `ref_topic_area` DISABLE KEYS */;
INSERT INTO `ref_topic_area` VALUES (1,'Air');
/*!40000 ALTER TABLE `ref_topic_area` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_load`
--

DROP TABLE IF EXISTS `temp_load`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_load` (
  `game_level` int(11) DEFAULT NULL,
  `topic_area` int(11) DEFAULT NULL,
  `question_type` int(11) DEFAULT NULL,
  `question` varchar(1000) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `a1` varchar(255) DEFAULT NULL,
  `a1_correct` varchar(45) DEFAULT NULL,
  `a2` varchar(255) DEFAULT NULL,
  `a2_correct` varchar(45) DEFAULT NULL,
  `a3` varchar(255) DEFAULT NULL,
  `a3_correct` varchar(45) DEFAULT NULL,
  `a4` varchar(255) DEFAULT NULL,
  `a4_correct` varchar(45) DEFAULT NULL,
  `right_wrong` varchar(1000) DEFAULT NULL,
  `sol_ref` varchar(1000) DEFAULT NULL,
  `source` varchar(1000) DEFAULT NULL,
  `reminder` varchar(1000) DEFAULT NULL,
  `temp_load_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`temp_load_id`),
  UNIQUE KEY `temp_load_id_UNIQUE` (`temp_load_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_load`
--

LOCK TABLES `temp_load` WRITE;
/*!40000 ALTER TABLE `temp_load` DISABLE KEYS */;
INSERT INTO `temp_load` VALUES (1,1,2,'Mitigation is the human intervention to reduce the sources or enhance the sinks of greenhouse gases.','','T','','','','','','','','','','IPCC, 2014: Summary for Policymakers, In: Climate Change 2014, Mitigation of Climate Change. Contribution of\rWorking Group III to the Fifth Assessment Report of the Intergovernmental Panel on Climate Change [Edenhofer,\rO., R. Pichs-Madruga, Y. Sokona, E. Farahani, S. Kadner, K. Seyboth, A. Adler, I. Baum, S. Brunner, P. Eickemeier, B.\rKriemann, J. Savolainen, S. Schl_mer, C. von Stechow, T. Zwickel and J.C. Minx (eds.)]. Cambridge University Press,\rCambridge, United Kingdom and New York, NY, USA.','\r',1,NULL),(1,1,2,'Climate change can impact human health, food security, biodiversity, local environmental quality, energy access, livelihoods, and equitable\rsustainable development','','T','','','','','','','','','','','\r',2,NULL),(1,1,2,'An \"increase\" or \"take over\" is when a species enters an new ecosystem and has no predators.','','T','','','','','','','','An invasive species is a species occurring as a result of human activities beyond its accepted normal distribution and which threatens valued environmental, agricultural and personal resources by the damage it causes.','','Australian Bureau of Statistics 1301.0- Year Book Australia, 2003 www.abs.gov.au/ausstats/abs@.nsf/46d1bc47ac9d0c7bca256c470025ff87/01cee6181b8b870cca256cae001599c2!OpenDocument ','\r',3,NULL),(1,1,2,'Green house gases can potentially acumulate and mix globally over time?','','T','','','','','','','','','','IPCC, 2014: Summary for Policymakers, In: Climate Change 2014, Mitigation of Climate Change. Contribution of\rWorking Group III to the Fifth Assessment Report of the Intergovernmental Panel on Climate Change [Edenhofer,\rO., R. Pichs-Madruga, Y. Sokona, E. Farahani, S. Kadner, K. Seyboth, A. Adler, I. Baum, S. Brunner, P. Eickemeier, B.\rKriemann, J. Savolainen, S. Schl_mer, C. von Stechow, T. Zwickel and J.C. Minx (eds.)]. Cambridge University Press,\rCambridge, United Kingdom and New York, NY, USA.','\r',4,NULL),(1,1,2,'A water shed is an area of land that drains water, sediment and dissolved materials to a common receiving body or outlet.','','T','','','','','','','','The James River watershed is the largest river watershed in Virginia, covering approximately ? of the state. The James River, along with the Potomac/ Shenandoah, Rappahanock and York watersheds all feed the larger Chesapeake Bay watershed.','','U.S. Environmental Protection Agency: Watershed Academy Web, Watershed Ecology\rwww.epa.gov/watertrain/ecology/ecology2.html  \rU.S. Environmental Protection Agency: Water, Watersheds-What is a Watershed?\rhttp://water.epa.gov/type/watersheds/whatis.cfm \rVirginia Department of Conservation and Recreation\rhttp://www.dcr.virginia.gov/soil_and_water/wsheds.shtml \rU.S. Environmental Protection Agency: Water, Healthy Watersheds http://www.epa.gov/nps/healthywatersheds/ \r','\r',5,NULL),(1,1,2,'A wetland is saturated land, like a marsh, where water is the determining factor of flora and fauna communities.','','T','','','','','','','','Wetlands are areas where water covers the soil or is present either at or near the surface of the soil all year or for varying periods of time during the year, including during the growing season.\r','','U.S. Environmental Protection Agency: Water, Wetlands _ Wetlands Definitions\rwww.epa.gov/owow/wetlands/what/definitions.html  \rU.S. Environmental Protection Agency: Water, Wetlands-What are Wetlands?\rwww.epa.gov/owow/wetlands/vital/what.html   \rU.S. Environmental Protection Agency: Wetlands Overview, What is a Wetland?\rhttp://water.epa.gov/type/wetlands/outreach/upload/overview.pdf \r','\r',6,NULL),(1,1,2,'Permeability is the measure of the ease with which a liquid (or gas) can flow through soil.','','T','','','','','','','','Permeability measures how easily a liquid (or gas) can flow through the soil while _porosity_ measures how much water soil can hold.\r','','U.S. Department of Agriculture, Natural Resources Conservation Service, Permeability Handout\rhttp://www.mt.nrcs.usda.gov/about/lessons/Lessons_Soil/permeability.html \r','\r',7,NULL),(1,1,2,'An estuary is an area where fresh and salt water mix, producing variations in salinity and high biological activity.','','T','','','','','','','','The Chesapeake Bay is largest estuary in the world; some other familiar examples of U.S. estuaries include San Francisco Bay, Puget Sound, Boston Harbor, and Tampa Bay.','','NOAA Estuary Education: National Estuarine Research Reserve System\rhttp://www.estuaries.gov \rThe Wild Classroom World Biomes: Biomes of the World, The Wild Classroom & Explore Biodiversity, Estuaries\rhttp://www.thewildclassroom.com/biomes/estuaries.html \r','\r',8,NULL),(1,1,2,'A vernal pool is a small, isolated wetlands that retain water on a seasonal basis','','T','','','','','','','','Vernal pools collect water during winter and spring rains, changing in volume in response to varying weather patterns. Climatic changes associated with each season cause dramatic changes in the appearance of vernal pools. During a single season, pools may fill and dry several times.\r','','The Vernal Pool: Information About Vernal Pools\rhttp://www.vernalpool.org/vpinfo_1.htm \rVirginia Department of Environmental Equality: Wetlands\rhttp://www.deq.virginia.gov/wetlands/wetlands.html \rFairfax County Virginia: Vernal Pools _ an Overlooked Natural Resource\rhttp://www.fairfaxcounty.gov/nvswcd/newsletter/vernalpools.htm\r','\r',9,NULL),(1,1,2,'A dam is a barrier controlling flow of water: a barrier of concrete or earth that is built across a river or stream to obstruct or control the flow of wate','','T','','','','','','','','There are over 79,000 large dams in the United States.\r','','Northwest  Power and Conservation Council: Energy, Guide to Major Dams, Grand Coulee Dam\rhttp://www.nwcouncil.org/energy/powersupply/dams/dam.asp?id=11 \rU.S. Army Corps of Engineers: National Inventory of Dams\rhttp://www.agc.army.mil/fact_sheet/nid.pdf \rU.S. Environmental Protection Agency: Polluted Runoff (Nonpoint Source Pollution) Management Measure for Erosion and Sediment Control - III. Dams Management Measures\rhttp://www.epa.gov/owow/NPS/MMGI/Chapter6/ch6-3a.html \r','\r',10,NULL),(1,1,2,'A Hurricane is a natural disaster that forms over land masses.','','F','','','','','','','','Hurricanes cause environmental and property damage through their storm surges, high winds, torrential rains, tornados, mudslides and coastal erosion. \r','','National Geographic: Hurricanes, Engine of Destruction\rhttp://environment.nationalgeographic.com/environment/natural-disasters/hurricane-profile/ \rNational Geographic, National Geographic News: Is Global Warming Hurricanes Worse?\rhttp://news.nationalgeographic.com/news/2005/08/0804_050804_hurricanewarming.html \r','\r',11,NULL),(1,1,2,'The water cycle deals with the natural circulation of water on planet Earth','','T','','','','','','','','The water cycle is the circuit of water movement from the atmosphere to the Earth and its return to the atmosphere through various stages or processes, such as precipitation, interception, runoff, infiltration, storage, evaporation and transpiration. ','','World Water Council\rhttp://www.worldwatercouncil.org/uploads/RTEmagicC_Water_cycle_01.JPG.jpg \rVirginia Department of Environmental Quality: Glossary of TMDL Terms\rhttp://www.deq.virginia.gov/tmdl/glossary.html#h \rUSGS Science for a Changing World: Water Science for Schools, Summary of the Water Cycle\rhttp://ga.water.usgs.gov/edu/watercyclesummary.html \rUSGS Science for a Changing World: Water Science for Schools, Where is Earth_s Water Located?\rhttp://ga.water.usgs.gov/edu/earthwherewater.html \r','\r',12,NULL),(1,1,2,'Water and wind do not contribute to soil erisoon?','','F','','','','','','','','Construction projects, farming and deforestation are major causes of erosion caused by humans..\r','','U.S. Environmental Protection Agency: Model Ordinances to Protect Local Resources _ Erosion and Sediment Control\rhttp://www.epa.gov/owow/nps/ordinance/erosion.htm \rInternational Erosion Control Association: Impact of Erosion Photos\rhttp://www.ieca.org/ImagesPhotogallery/photo_todd.jpg \rNASA GODDARD SPACE FLIGHT CENTER WALLOPS FLIGHT FACILITY STORMWATER POLLUTION PREVENTION PROGRAM FACT SHEET: Erosion and Sediment Control\rhttp://sites.wff.nasa.gov/code250/docs/soil_erosion.pdf \r','\r',13,NULL),(1,1,2,'Sedimentation is the deposition of particles from a fluid such as water','','T','','','','','','','','Large-scale development means that a lot of land clearing and grading occurs. If the area has sloping land, soils that erode easily, and receives frequent periods of heavy rainfall, then water quality can be affected, usually in a negative way.\r','','USGS Science for the Changing World, Water Science for Schools: The effect of Urbanization on water Quality: Erosion and Sedimentation\rhttp://ga.water.usgs.gov/edu/urbansed.html \r','\r',14,NULL);
/*!40000 ALTER TABLE `temp_load` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(255) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `city` varchar(100) DEFAULT NULL,
  `state` varchar(2) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `admin` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (16,'justin','Pa$$w0r6','Justin','Kent','Reston','VA','justinrkent@gmail.com',1),(28,'cheneycs','jordan','craig','cheney','Falls Church','VA','cheneycraig307@gmail.com',0),(29,'joe','password','Joe','Smith','Reston','VA','asdf@asdf.com',0),(30,'jprewitt','Chandler2','Jerry','Prewitt','Reston','VA','jprewitt@vets-inc.com',0),(31,'justin2','password','Justin','Kent','Reston','VA','justin_kent@sra.com',0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-10 14:02:01
