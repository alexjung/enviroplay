/* add short_name column */
ALTER TABLE ref_level
ADD level_short_name	VARCHAR(20);

/* populate short_name column */
UPDATE ref_level
SET level_short_name = 'High School'
WHERE level_id = 1;

UPDATE ref_level
SET level_short_name = 'Grades 7-8'
WHERE level_id = 2;

UPDATE ref_level
SET level_short_name = 'Grades 4-6'
WHERE level_id = 3;

UPDATE ref_level
SET level_short_name = 'Grades K-3'
WHERE level_id = 4;
