<?php session_start(); ?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>EnviroPlay</title>

<link rel="stylesheet" type="text/css" href="_style/site.css">
<link rel="stylesheet" type="text/css" href="_style/main.css">
</head>

<body>
<div class="background">
  <div class="header"><?php include('_header.php'); ?></div>
  <div class="clear-float"></div>
    
  <div class="signin">
        <div class="signin-form">
            <?php
            
            if (isset($_SESSION['loggedin'])) {
                echo "Welcome " .$_SESSION['fname'] . "!";
            } else {
                echo    '<h3>Sign in</h3>
						<form name="signin" method="post" action="includes/login.php">
                        <h4>Username</h4>
                        <input type="text" id="myusername" name="myusername"></input>
                        <h4>Password</h4>
                        <input type="password" id="mypassword" name="mypassword"></input>';
                if (isset($_SESSION['loginfailed'])) {
                    echo "<div class='error'>Login Failed  </div>";
                    unset($_SESSION['loginfailed']);
                } elseif (isset($_SESSION['message'])) {
                    echo '<div class="message">'.$_SESSION['message'].'</div>';
                    unset($_SESSION['message']);
                }
                echo    '<span class="black-text"><a href="forgotPassword.php">forgot username/password</a></span>
                        <br/>
                        <div class="signin-buttons">
                            <input type="submit" value="Sign in" id="create-account-button" name="Submit"></input>
                            <span class="cancel-link"><a href="register.php">Register</a></span>
                        </div>
                      </form>';
            }
            ?>
            
        </div>
    </div>
    <?php
    if (isset($_SESSION['loggedin'])) {
    echo
	'<div class="game-level">
  	  <div class="select-game-level">
	        <h3>Select Game Level</h3>
    		<form method="post" action="quiz.php">
				<input type="submit" value="Grades K-3" class="grade-submit">
				<input type="text" name="gradelevelid" id="gradelevelid" value="4" hidden="true">
			</form>
			<form method="post" action="quiz.php">
				<input type="submit" value="Grades 4-6" class="grade-submit">
				<input type="text" name="gradelevelid" id="gradelevelid" value="3" hidden="true">
			</form>
			<form method="post" action="quiz.php">
				<input type="submit" value="Grades 7-8" class="grade-submit">
				<input type="text" name="gradelevelid" id="gradelevelid" value="2" hidden="true">
			</form>
			<form method="post" action="quiz.php">
				<input type="submit" value="High School" class="grade-submit">
				<input type="text" name="gradelevelid" id="gradelevelid" value="1" hidden="true">
			</form>
        </div>
	</div>
	<div class="instructions">
    	<div class="how-to-play">
        	<h2>How to Play!</h2>
            This is the area where you explain how to play.  This is the area where you explain how to play.  This is the area where you explain how to play. Test
        </div>
        
 	</div>';
    } 
	else { 
		echo
		'<div class="welcome">
			<div class="welcome-text">
				<h2>Welcome!</h2>
				This is the area where we write intro text about the games. This is the area where we write intro text about the games. This is the area where we write intro text about the games.
			</div>
		</div>';	
    }
    ?>
    <div class="clear-float"></div>
    <div class="footer"><?php include('_footer.php'); ?></div>
</div>
</body>
</html>