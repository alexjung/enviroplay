<?php
include_once '../../includes/utilityFunctions.php';

if (!$adminUserExists){
    $_SESSION['systemMessage'] = [['warning', 'Please sign in before viewing that page.']];
    $_SESSION['userIdx'] = 0;

    header('Location: ../../index.php');

    exit;
}

// validate submissions
$notRequired = ['right_wrong_desc', 'source', 'reminder'];

switch($_POST['question_type_id']) {
    case 1:
    case 3:
        array_push($notRequired, 'tf_ans');
        break;

    case 2:
        for ($i=0; $i < 4; $i++){
            array_push($notRequired, 'ans' . $i);
        }
        break;

}

$pass = true;
foreach($_POST as $key => $value){
    $_SESSION['admin']['question'][$key] = $value;

    if (!in_array($key, $notRequired) && $value == ''){
        $pass = false;
    }
}
if (!$pass){
    $_SESSION['systemMessage'] = [['warning', 'Please enter values for all required fields.']];

    header('Location: ../addQuestion.php');
    exit;
}

DB::startTransaction();

try {
    $tf_ans = ($_POST['question_type_id'] == 2) ? $_POST['tf_ans'] : NULL;

    DB::insert('question', [
        'level_id' => $_POST['level_id'],
        'topic_area_id' => $_POST['topic_area_id'],
        'question_type_id' => $_POST['question_type_id'],
        'question' => $_POST['question'],
        'right_wrong_desc' => $_POST['right_wrong_desc'],
        'source' => $_POST['source'],
        'reminder' => $_POST['reminder'],
        'tf_ans' => $tf_ans
    ]);

    $qId = DB::insertId();

    switch($_POST['question_type_id']){
        case 1:
        case 3:
            for ($i=0; $i < 4; $i++){
                if ($_POST['question_type_id'] == 1){
                    $correct = ($i == 0) ? 'Y' : 'N';
                }
                elseif ($_POST['question_type_id'] == 3){
                    $correct = $i + 1;
                }
                DB::insert('answer', [
                    'question_id' => $qId,
                    'answer' => $_POST['ans' . $i],
                    'correct' => $correct
                ]);
            }

            break;
    }

    DB::commit();

    $_SESSION['systemMessage'] = [['success', 'Your question was successfully added. <a href="#q-' . $qId . '" class="scroll-to-link">Scroll to your added question</a>.']];

    unset($_SESSION['admin']['question']);
}
catch (Exception $e){
    DB::rollback();

    $_SESSION['systemMessage'] = [['danger', 'There was a problem with adding your question. Please try again.']];
}

header('Location: ../listQuestions.php');
exit;
?>