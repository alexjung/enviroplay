<?php
include_once '../../includes/utilityFunctions.php';

if (!$adminUserExists){
    $_SESSION['systemMessage'] = [['warning', 'Please sign in before viewing that page.']];
    $_SESSION['userIdx'] = 0;

    header('Location: ../../index.php');

    exit;
}

$questionID = $_POST['questionID'];
$questionType = $_POST['questionType'];
$question = $_POST['question'];
$answerInfo = $_POST['answerInfo'];

DB::update('question', array('question' => $question, 'right_wrong_desc' => $answerInfo), 'question_id=%i', $questionID);

switch($questionType):
    case 1:
    case 3:
        $ansIDs = unserialize($_POST['ansIDs']);

        foreach ($ansIDs as $ans) {
            $answerID = 'ans' . $ans;
            $answer = $_POST[$answerID];
            DB::update('answer', array('answer' => $answer), 'answer_id=%i', $ans);
        }

        break;

    case 2:
        $tf = $_POST['tf'];

        DB::update('question', array('tf_ans' => $tf), 'question_id=%i', $questionID);

        break;

endswitch;

$_SESSION['systemMessage'] = [['success', 'Question Edited! <a href="#q-' . $questionID . '" class="scroll-to-link">Go to edited question</a>.']];

header('Location: ../listQuestions.php');
exit;
?>