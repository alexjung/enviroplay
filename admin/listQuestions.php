<?php
include_once '../includes/utilityFunctions.php';

if (!$adminUserExists){
    $_SESSION['systemMessage'] = [['warning', 'Please sign in before viewing that page.']];
    $_SESSION['userIdx'] = 0;

    header('Location: ' . ROOT_DIR_PATH . 'index.php');
    exit;
}

function convert_to_css_class($val){
    return str_replace([' ', '/'], "-", strtolower($val));
}

$questionsDB = DB::query('SELECT q.*, rq.*, rl.*, rt.* FROM ref_level rl JOIN question q ON rl.level_id = q.level_id JOIN ref_topic_area rt ON q.topic_area_id = rt.topic_area_id JOIN ref_question_type rq ON q.question_type_id = rq.question_type_id ORDER BY q.question_id');

$qGrades = DB::query('SELECT r.* FROM ref_level r JOIN question q ON r.level_id = q.level_id ORDER BY r.level_id DESC');
$qTopics = DB::query('SELECT r.* FROM ref_topic_area r JOIN question q ON r.topic_area_id = q.topic_area_id ORDER BY r.topic_area');
$qTypes = DB::query('SELECT r.* FROM ref_question_type r JOIN question q ON r.question_type_id = q.question_type_id ORDER BY r.question_type_id');

$pageTitle = "EnviroPlay Admin - Questions List";

require_once '../includes/template/header.php';
?>
<div class="container main-container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>

<?php
if (isset($_SESSION['message'])) {
    echo '<div class="message">'.$_SESSION['message'].'</div>';
    unset($_SESSION['message']);
}
?>
    <div class="well well-lg clearfix admin-question-list">
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                Grade Level: <span class="selection-gradelevel">All</span> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-gradelevel" role="menu">
<?php
$str = "";
foreach ($qGrades as $row):
    if ($str != $row['level_short_name']):
        $str = $row['level_short_name'];
        $str_css = convert_to_css_class($str);
        $str_id = $str_css . '-' . $row['level_id'];
?>
                <li><a href="#"><input type="checkbox" id="<?php echo $str_id; ?>" value="<?php echo $str_css; ?>"> <label for="<?php echo $str_id; ?>"><?php echo $str; ?></label></a></li>
<?php
    endif;
endforeach;
?>
            </ul>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                Topic: <span class="selection-topicarea">All</span> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-topic-area" role="menu">
<?php
$str = "";
foreach ($qTopics as $row):
    if ($str != $row['topic_area']):
        $str = $row['topic_area'];
        $str_css = convert_to_css_class($str);
        $str_id = $str_css . $row['topic_area_id'];
?>
                <li><a href="#"><input type="checkbox" id="<?php echo $str_id; ?>" value="<?php echo $str_css; ?>"> <label for="<?php echo $str_id; ?>"><?php echo $str; ?></label></a></li>
<?php
    endif;
endforeach;
?>
            </ul>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                Type: <span class="selection-questiontype">All</span> <span class="caret"></span>
            </button>
            <ul class="dropdown-menu dropdown-menu-question-type" role="menu">
<?php
$str = "";
foreach ($qTypes as $row):
    if ($str != $row['question_type']):
        $str = $row['question_type'];
        $str_css = convert_to_css_class($str);
        $str_id = $str_css . $row['question_type_id'];
?>
                <li><a href="#"><input type="checkbox" id="<?php echo $str_id; ?>" value="<?php echo $str_css; ?>"> <label for="<?php echo $str_id; ?>"><?php echo $str; ?></label></a></li>
<?php
    endif;
endforeach;
?>
            </ul>
        </div>
        <div class="pull-right">
            <a href="addQuestion.php"><button type="button" class="btn btn-lg btn-link">+ Add New Question</button></a>
        </div>
        <table id="q-list" class="table table-bordered table-hover table-condensed" style="margin-top:1em;">
            <thead>
                <th id="q-id" style="width:0%;">ID</th>
                <th id="q-text" style="width:60%;">Question</th>
                <th id="q-grade" style="width:15%;">Level</th>
                <th id="q-topic" style="width:10%;">Topic</th>
                <th id="q-type" style="width:10%;">Type</th>
                <th id="q-act" style="width:5%;">Action</th>
            </thead>
            <tbody>
<?php
foreach ($questionsDB as $row):
?>
                <tr id="q-<?php echo $row['question_id']; ?>"class="<?php echo convert_to_css_class($row['level_short_name']) . ' ' . convert_to_css_class($row['topic_area']) . ' ' . convert_to_css_class($row['question_type']); ?>">
                    <td headers="q-id"><?php echo $row['question_id']; ?></td>
                    <td headers="q-text"><?php echo $row['question']; ?></td>
                    <td headers="q-grade"><?php echo $row['level_short_name']; ?></td>
                    <td headers="q-topic"><?php echo $row['topic_area']; ?></td>
                    <td headers="q-type"><?php echo $row['question_type']; ?></td>
                    <td headers="q-act" class="action"><a href="editQuestion.php?questionID=<?php echo $row['question_id']; ?>">Edit</a> / <a href="deleteQuestion.php?questionID=<?php echo $row['question_id']; ?>" class="delete-question">Delete</a></td>
                </tr>
<?php
endforeach;
?>
            </tbody>
        </table>
        <a href="admin.php"><button type="button" class="btn btn-lg btn-link">Cancel</button></a>
    </div>
</div>
<?php
// set up page JS to minify
$pageJs = <<<EOSCRIPT
var adminUtils = {
    parseMenu: function(whatBase){
        \$tmp = \$('ul.dropdown-menu-' + whatBase + ' input:checkbox:checked');

        if (\$tmp.length){

        }
    },
    setMenuDisplay: function(whatObj){
        for(var key in whatObj){
            var \$options = \$('ul.dropdown-menu-' + key + ' li');
            var txt = (whatObj[key].length && whatObj[key].length != \$options.length) ? whatObj[key].join() : 'All';
            \$('.selection-' + key).html(txt);
        }
    }
};
\$('.dropdown-menu a').click(function(e){
    var classes = [],
        classList = "",
        selectionDisplay = {
            gradelevel: [],
            topicarea: [],
            questiontype: []
        },
        \$grades,
        \$topics;

    \$grades = \$('ul.dropdown-menu-gradelevel input:checkbox:checked');
    if (\$grades.length){
        \$grades.each(function(i){
            var label = \$('label[for="' + this.id + '"]').html();
            selectionDisplay["gradelevel"].push(label);

            classList = "." + \$(this).val();

            var \$topics = \$('ul.dropdown-menu-topic-area input:checkbox:checked');
            if (\$topics.length){
                \$topics.each(function(j){
                    var label = \$('label[for="' + this.id + '"]').html();
                    if (i == 0){
                        selectionDisplay["topicarea"].push(label);
                    }
                    classList += "." + \$(this).val();

                    \$('ul.dropdown-menu-question-type input:checkbox:checked').each(function(){
                        var label = \$('label[for="' + this.id + '"]').html();
                        if (j == 0){
                            selectionDisplay["questiontype"].push(label);
                        }
                        classList += "." + \$(this).val();
                    });
                });
            }
            else {
                \$('ul.dropdown-menu-question-type input:checkbox:checked').each(function(){
                    var label = \$('label[for="' + this.id + '"]').html();
                    if (i == 0){
                        selectionDisplay["questiontype"].push(label);
                    }
                    classList += "." + \$(this).val();
                });
            }

            classes.push(classList);
        });
    }
    else {
        var \$topics = \$('ul.dropdown-menu-topic-area input:checkbox:checked');
        if (\$topics.length){
            \$topics.each(function(){
                var label = \$('label[for="' + this.id + '"]').html();
                selectionDisplay["topicarea"].push(label);
                classList = "." + \$(this).val();

                \$('ul.dropdown-menu-question-type input:checkbox:checked').each(function(){
                    var label = \$('label[for="' + this.id + '"]').html();
                    if (i == 0){
                        selectionDisplay["questiontype"].push(label);
                    }
                    classList += "." + \$(this).val();
                });

                classes.push(classList);
            });
        }
        else {
            \$('ul.dropdown-menu-question-type input:checkbox:checked').each(function(){
                var label = \$('label[for="' + this.id + '"]').html();
                selectionDisplay["questiontype"].push(label);
                classList = "." + \$(this).val();

                classes.push(classList);
            });
        }
    }
    if (classes.length){
        \$('table#q-list tbody tr').each(function(){
            var \$thisRow = \$(this);
            if (\$thisRow.hasClasses(classes)){
                \$thisRow.show();
            }
            else {
                \$thisRow.hide();
            }
        });
    }
    else {
        \$('table#q-list tbody tr').show();
    }
    adminUtils.setMenuDisplay(selectionDisplay);
});

\$('.delete-question').click(function(e){
    e.preventDefault();

    if (confirm('Are you sure you want to permanently delete this question?')){
        document.location.href = \$(this).prop('href');
    }
});
EOSCRIPT;
// include minifier
include_once "../includes/JShrink.php";

// minify JS and wrap in script tag
$pageJs = "<script type=\"text/javascript\">\n" . \JShrink\Minifier::minify($pageJs) . "\n</script>\n";

require_once '../includes/template/footer.php';
?>
