<?php
include_once '../includes/utilityFunctions.php';

if (!$adminUserExists){
    $_SESSION['systemMessage'] = [['warning', 'Please sign in before viewing that page.']];
    $_SESSION['userIdx'] = 0;

    header('Location: ' . ROOT_DIR_PATH . 'index.php');
    exit;
}

$pageTitle = "EnviroPlay Admin";

require_once '../includes/template/header.php';
?>
<div class="container main-container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>

    <div class="well well-lg clearfix">
      	<ul class="holder">
            <li><a href="listQuestions.php">Edit Questions</a></li>
            <li><a href="addQuestion.php">Add Question</a></li>
            <li>Import Questions</li>
        </ul>
    </div>
</div>
<?php
$pageJs = <<<EOSCRIPT
<script type="text/javascript">
\$('.scroll-to-link').click(function(e){
    e.preventDefault();
});
</script>
EOSCRIPT;

require_once '../includes/template/footer.php';
?>
