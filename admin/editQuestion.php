<?php
include_once '../includes/utilityFunctions.php';

if (!$adminUserExists){
    $_SESSION['systemMessage'] = [['warning', 'Please sign in before viewing that page.']];
    $_SESSION['userIdx'] = 0;

    header('Location: ' . ROOT_DIR_PATH . 'index.php');
    exit;
}
$questionDB = DB::queryFirstRow('SELECT q.*, rq.question_type FROM question q, ref_question_type rq WHERE q.question_id = %i AND q.question_type_id = rq.question_type_id',$_GET['questionID']);

$questionID = $questionDB['question_id'];
$question = $questionDB['question'];
$answerInfo = $questionDB['right_wrong_desc'];
$questionType = $questionDB['question_type_id'];
$questionTypeName = $questionDB['question_type'];
$questionGrade = $questionDB['level_id'];
$questionTopic = $questionDB['topic_area_id'];

// get all topics from the DB
$qTopics = DB::query("SELECT * FROM ref_topic_area ORDER BY topic_area");

// get all grades from the DB
$qGrades = DB::query("SELECT * FROM ref_level ORDER BY level_id DESC");

switch($questionType){
    case 1:
    case 3:
        $answersDB = DB::query('SELECT * FROM answer WHERE question_id = %i', $questionID);

        if ($questionType == 1){
            foreach ($answersDB as $row) {
                $answers[$row['answer_id']] = $row['answer'];
                if($row['correct'] == 'Y') {
                    $correctAnswer = trim($row['answer_id']);
                }
            }
        }
        else {
            $ordinal = [
                '1st',
                '2nd',
                '3rd',
                '4th'
            ];

            $correctAnswer = "";
            foreach ($answersDB as $row) {
                $answers[$row['answer_id']] = $row['answer'];
            }
        }

        break;

    case 2:
        $correctAnswer = $questionDB['tf_ans'];

        break;

}

$correctStyle = "color:#008a00;";

$pageTitle = "EnviroPlay Admin - Edit Questions";

require_once '../includes/template/header.php';
?>

<div class="container main-container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>

    <div class="well well-lg clearfix">
        <h2 class="first" style="margin-bottom:1em;">"<?php echo $questionTypeName; ?>" Question Type:</h2>
<?php
// display intro text for multiple choice question
if ($questionType == 1):
?>
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm req-intro" style="background-color:#eee;<?php echo $correctStyle; ?>"><strong><i class="glyphicon glyphicon-ok form-control-feedback"></i> Correct Answer</strong></div>
            </div>
        </div>
<?php
endif;
?>
        <div class="row">
            <form name="editQuestion" role="form" action="includes/editQuestionPost.php" method="post" class="form-horizontal" style="padding-right:1em;">
                <input type="hidden" name="questionID" id="questionID" value="<?php echo $questionID; ?>">
                <input type="hidden" name="questionType" id="questionType" value="<?php echo $questionType; ?>">

                <div class="form-group">
                    <label for="grade" class="col-xs-2">Grade:</label>
                    <div class="input-group col-xs-10">
                        <select name="grade" id="grade">
<?php
foreach ($qGrades as $row):
?>
                                <option value="<?php echo $row['level_id']; ?>"<?php echo ($row['level_id'] == $questionGrade) ? 'selected' : ''; ?>><?php echo $row['level_short_name']; ?></option>
<?php
endforeach;
?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="topic" class="col-xs-2">Topic:</label>
                    <div class="input-group col-xs-10">
                        <select name="topic" id="topic">
<?php
foreach ($qTopics as $row):
?>
                                <option value="<?php echo $row['topic_area_id']; ?>"<?php echo ($row['topic_area_id'] == $questionTopic) ? 'selected' : ''; ?>><?php echo $row['topic_area']; ?></option>
<?php
endforeach;
?>
                        </select>
                    </div>
                </div>
                <hr class="col-xs-offset-2">
                <div class="form-group">
                    <label for="question" class="col-xs-2">Question:</label>
                    <div class="input-group col-xs-10">
                        <textarea class="form-control" name="question" id="question" placeholder="Question" required><?php echo trim($question); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="answerInfo" class="col-xs-2">Answer Info:</label>
                    <div class="input-group col-xs-10">
                        <textarea class="form-control" name="answerInfo" id="answerInfo" placeholder="Answer Info" required><?php echo trim($answerInfo); ?></textarea>
                    </div>
                </div>
<?php
// display different form for each question type
switch($questionType):
    // multiple choice & sort list
    case 1:
    case 3:
        echo "                <hr class=\"col-xs-offset-2\">\n";
        $ansIDs = [];
        $cntr = 0;
        foreach ($answers as $key => $ans):
            $cntr++;
            array_push($ansIDs, $key);
?>
                <div class="form-group">
                    <label for="ans<?php echo $key; ?>" class="col-xs-2"><?php echo ($questionType == 1) ? 'Answer ' . $cntr : $ordinal[$cntr - 1] . ' Position'; ?>:</label>
                    <div class="input-group col-xs-10">
                        <textarea class="form-control" name="ans<?php echo $key; ?>" id="ans<?php echo $key; ?>" placeholder="Answer <?php echo $cntr; ?>" required><?php echo trim($ans); ?></textarea>
<?php
            if ($key == $correctAnswer):
?>
                        <span class="input-group-addon" style="<?php echo $correctStyle; ?>"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
<?php
            endif;
?>
                    </div>
                </div>
<?php
        endforeach;

        $ansIDsString = serialize($ansIDs);
        echo '                <input type="hidden" name="ansIDs" id="ansIDs" value="'.$ansIDsString.'">';

        break;

    // true or false
    case 2:
?>
                <div class="form-group">
                    <label class="col-xs-2">Answer:</label>
                    <div class="input-group col-xs-10">
<?php
        $validTf = ['True', 'False'];
        foreach ($validTf as $tf):
            $firstLtr = substr($tf, 0, 1);
?>
                        <input type="radio" name="tf" id="tf_<?php echo $firstLtr; ?>" value="<?php echo $firstLtr; ?>"<?php echo ($correctAnswer == $firstLtr) ? ' checked' : ''; echo ($correctAnswer == $firstLtr) ? ' checked' : ''; ?>> <label for="tf_<?php echo $firstLtr; ?>"><?php echo $tf; ?></label>&nbsp;&nbsp;
<?php
        endforeach;
?>
                    </div>
                </div>
<?php
        break;

endswitch;
?>

                <div class="col-xs-10 col-xs-offset-2">
                    <button type="submit" class="btn btn-default">Update Question</button> <a href="listQuestions.php"><button type="button" class="btn btn-lg btn-link">Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
</div>

<?php
$pageJs = <<<ENDOFSCRIPT
<script type="text/javascript" src="../js/jquery.autosize.min.js"></script>
<script language="javascript" type="text/javascript">
$('textarea').autosize();
</script>
ENDOFSCRIPT;

require_once '../includes/template/footer.php';
?>