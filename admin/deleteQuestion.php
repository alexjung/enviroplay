<?php
include_once '../includes/utilityFunctions.php';

if (!$adminUserExists){
    $_SESSION['systemMessage'] = [['warning', 'Please sign in before viewing that page.']];
    $_SESSION['userIdx'] = 0;

    header('Location: ' . ROOT_DIR_PATH . 'index.php');
    exit;
}

$qId = $_GET['questionID'];
if (is_int($qId)){
    DB::startTransaction();

    try {
        DB::delete('answer', "question_id=%i", $qId);
        DB::delete('question', "question_id=%i", $qId);

        DB::commit();

        $_SESSION['systemMessage'] = [['success', 'A question was successfully deleted.']];
    }
    catch (Exception $e){
        DB::rollback();

        $_SESSION['systemMessage'] = [['danger', 'There was a problem with a deletion. Please try again.']];
    }

}
else {
    $_SESSION['systemMessage'] = [['warning', 'Please select a question to delete first.']];
}

header('Location: listQuestions.php');

exit;
?>