<?php
include_once '../includes/utilityFunctions.php';

if (!$adminUserExists){
    $_SESSION['systemMessage'] = [['warning', 'Please sign in before viewing that page.']];
    $_SESSION['userIdx'] = 0;

    header('Location: ' . ROOT_DIR_PATH . 'index.php');
    exit;
}

if (!isset($_SESSION['admin']) || !isset($_SESSION['admin']['question'])){
    $_SESSION['admin'] = [
        "question" => [
            "question_type_id" => "",
            "level_id" => "",
            "topic_area_id" => "",
            "question" => "",
            "right_wrong_desc" => "",
            "source" => "",
            "reminder" => "",
            "ans0" => "",
            "ans1" => "",
            "ans2" => "",
            "ans3" => "",
            "tf_ans" => ""
        ]
    ];
}

// get all question types
$qQuestionTypes = DB::query("SELECT * FROM ref_question_type ORDER BY question_type");
// get all topics from the DB
$qTopics = DB::query("SELECT * FROM ref_topic_area ORDER BY topic_area");
// get all grades from the DB
$qGrades = DB::query("SELECT * FROM ref_level ORDER BY level_id DESC");

$correctStyle = "color:#008a00;";

$pageTitle = "EnviroPlay Admin - Add Question";

require_once '../includes/template/header.php';
?>
<div class="container main-container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>

    <div class="well well-lg clearfix">
        <div class="row">
            <form name="addQuestion" role="form" action="includes/addQuestionPost.php" method="post" class="form-horizontal" style="padding-right:1em;">
                <div class="form-group">
                    <label for="grade" class="col-xs-2">Question Type:</label>
                    <div class="input-group col-xs-10">
                        <select name="question_type_id" id="question_type_id">
<?php
foreach ($qQuestionTypes as $row):
?>
                                <option value="<?php echo $row['question_type_id']; ?>"<?php echo ($row['question_type_id'] == $_SESSION['admin']['question']['question_type_id']) ? 'selected' : ''; ?>><?php echo $row['question_type']; ?></option>
<?php
endforeach;
?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="grade" class="col-xs-2">Grade:</label>
                    <div class="input-group col-xs-10">
                        <select name="level_id" id="level_id">
                            <?php
                            foreach ($qGrades as $row):
                                ?>
                                <option value="<?php echo $row['level_id']; ?>"<?php echo ($row['level_id'] == $_SESSION['admin']['question']['level_id']) ? 'selected' : ''; ?>><?php echo $row['level_short_name']; ?></option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="grade" class="col-xs-2">Topic:</label>
                    <div class="input-group col-xs-10">
                        <select name="topic_area_id" id="topic_area_id">
                            <?php
                            foreach ($qTopics as $row):
                                ?>
                                <option value="<?php echo $row['topic_area_id']; ?>"<?php echo ($row['topic_area_id'] == $_SESSION['admin']['question']['topic_area_id']) ? 'selected' : ''; ?>><?php echo $row['topic_area']; ?></option>
                            <?php
                            endforeach;
                            ?>
                        </select>
                    </div>
                </div>
                <hr class="col-xs-offset-2">
                <div class="form-group">
                    <label for="question" class="col-xs-2">Question:</label>
                    <div class="input-group col-xs-10">
                        <textarea class="form-control" name="question" id="question" placeholder="Question" required><?php echo trim($_SESSION['admin']['question']['question']); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="right_wrong_desc" class="col-xs-2">Answer Info:</label>
                    <div class="input-group col-xs-10">
                        <textarea class="form-control" name="right_wrong_desc" id="right_wrong_desc" placeholder="Answer Info" required><?php echo trim($_SESSION['admin']['question']['right_wrong_desc']); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="source" class="col-xs-2">Source:</label>
                    <div class="input-group col-xs-10">
                        <textarea class="form-control" name="source" id="source" placeholder="Source" required><?php echo trim($_SESSION['admin']['question']['source']); ?></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="reminder" class="col-xs-2">Reminder:</label>
                    <div class="input-group col-xs-10">
                        <textarea class="form-control" name="reminder" id="reminder" placeholder="Reminder" required><?php echo trim($_SESSION['admin']['question']['reminder']); ?></textarea>
                    </div>
                </div>
                <hr class="col-xs-offset-2">
                <div class="q-type-div q-type-div-1">
<?php
for ($i = 0; $i < 4; $i++):
?>
                    <div class="form-group">
                        <label for="ans<?php echo $i; ?>" class="q-type-label q-type-label-<?php echo $i; ?> col-xs-2">Answer <?php echo $i + 1; ?>:</label>
                        <div class="input-group col-xs-10">
                            <textarea class="form-control" name="ans<?php echo $i; ?>" id="ans<?php echo $i; ?>" placeholder="Answer <?php echo $i + 1; ?>" required><?php //echo trim($ans); ?></textarea>
<?php
    if ($i == 0):
?>
                                <span id="q-type-1-addon" class="input-group-addon" style="<?php echo $correctStyle; ?>"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
<?php
    endif;
?>
                        </div>
                    </div>
<?php
endfor;
?>
                </div>
                <div class="q-type-div q-type-div-2">
                    <div class="form-group">
                        <label class="col-xs-2">Answer:</label>
                        <div class="input-group col-xs-10">
<?php
$validTf = ['True', 'False'];
foreach ($validTf as $tf):
    $firstLtr = substr($tf, 0, 1);
?>
                                <input type="radio" name="tf_ans" id="tf_<?php echo $firstLtr; ?>" value="<?php echo $firstLtr; ?>"<?php //echo ($correctAnswer == $firstLtr) ? ' checked' : ''; echo ($correctAnswer == $firstLtr) ? ' checked' : ''; ?>> <label for="tf_<?php echo $firstLtr; ?>"><?php echo $tf; ?></label>&nbsp;&nbsp;
<?php
endforeach;
?>
                        </div>
                    </div>
                </div>

                <div class="col-xs-10 col-xs-offset-2">
                    <button type="submit" class="btn btn-default">Add Question</button> <a href="listQuestions.php"><button type="button" class="btn btn-lg btn-link">Cancel</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<?php
$pageJs = <<<EOSCRIPT
<script type="text/javascript">
adminUtils = {
    displayQuestions: function(whatQuestionType){
        switch(whatQuestionType){
            case 1:
            case 3:
                \$('.q-type-div-2').hide();
                \$('.q-type-div-1').show();

                if (whatQuestionType == 1){
                    \$('#q-type-1-addon').show();
                    \$('.q-type-label').each(function(i){
                        var label = 'Answer ' + (i+1);
                        \$(this).html(label + ':');
                        \$('#ans' + i).prop('placeholder', label);
                    });
                }
                else {
                    \$('#q-type-1-addon').hide();

                    var ordinals = [
                        '1st',
                        '2nd',
                        '3rd',
                        '4th'
                    ];
                    \$('.q-type-label').each(function(i){
                        var label = ordinals[i] + ' Position';
                        \$(this).html(label + ':');
                        \$('#ans' + i).prop('placeholder', label);
                    });
                }
            break;

            case 2:
                \$('.q-type-div-1').hide();
                \$('.q-type-div-2').show();
            break
        }
    }
};
\$('#question_type_id').change(function(){
    var \$this = \$(this);

    adminUtils.displayQuestions(\$this.val() * 1);
}).change();
</script>
EOSCRIPT;

require_once '../includes/template/footer.php';
?>
