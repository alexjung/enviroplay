// create object
var enviroplay = {
    init: function(){
        this.setColumnHeights();
        this.redirectQuit();
    },
    // dynamically load JS (with caching)
    getCachedScript: function( url, options ) {
        // Allow user to set any option except for dataType, cache, and url
        options = $.extend( options || {}, {
            dataType: "script",
            cache: true,
            url: url
        });

        // Return the jqXHR object so we can chain callbacks
        return jQuery.ajax( options );
    },
    redirectQuit: function(){
        $().click(function(e){
            e.preventDefault();
        });
    },
    // animate quiz round modal notification
    sendObjectTo: function(whatOriginObj, whatDestinationObj){
        //
        if (whatDestinationObj && whatOriginObj){
            whatOriginObj.offset({
                top: whatOriginObj.offset().top,
                left: whatOriginObj.offset().left
            }).css({
                'opacity': '0.5',
                'position': 'absolute',
                'height': '300px',
                'width': '300px',
                'z-index': '100'
            }).animate({
                'top': whatDestinationObj.offset().top + 10,
                'left': whatDestinationObj.offset().left + 10,
                'width': whatDestinationObj.width(),
                'height': whatDestinationObj.height()
            }, 1500, 'easeInOutExpo', function(){
                whatOriginObj.detach();
            });
        }
    },
    // keeps well heights in sync
    setColumnHeights: function(){
        // only sync if not phone/vertical tablet view
        if ($(window).width() >= 768) {
            $(".equalizer").each(function () {
                var heights = $(this).find(".watch").map(function () {
                        return $(this).height();
                    }).get(),

                    maxHeight = Math.max.apply(null, heights);
                $(".watch").height(maxHeight);
            });
        }
        else {
            $(".watch").height('auto');
        }
    }
};
$.fn.extend({
    hasClasses: function (selectors) {
        var $this = $(this);
        for (var i in selectors) {
            var tmp = selectors[i].replace(".", "").split(".");
            var hasAll = true;
            for (var j in tmp){
                if (!$this.hasClass(tmp[j])){
                    hasAll = false;
                    break;
                }
            }

            if (hasAll){
                return true;
            }
        }
        return false;
    }
});
// scripts needing ready event
$(document).ready(function(){
    enviroplay.setColumnHeights();
    $(window).resize(function() {
        enviroplay.setColumnHeights();
    });

    // remove default click events to the quiz nav in header; attach form submit instead
    $('.navbar-quiz-nav').click(function(e){
        e.preventDefault();

        var classNames = $(this).attr('class').split(" ");
        for ( var i = 0, l = classNames.length; i<l; ++i ) {
            if (classNames[i].indexOf('grade-id-') === 0){
                var newVal = classNames[i].replace('grade-id-', '');
                break;
            }
        }
        $('#navbar-quiz-gradelevelid').val(newVal);
        $('#navbar-quiz-form').submit();
    })
});
