<?php session_start();?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Forgot Password</title>

<link rel="stylesheet" type="text/css" href="site.css">
<link rel="stylesheet" type="text/css" href="forgotPassword.css">

</head>

<?php
$email = "";
$emailErr = "";
$emailNotValid = true;
$valid = true;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	
	if (empty($_POST["email"])) {	
		$emailErr = "Email is required<br/>";
		$valid = false;
	} else {
		$email = test_input($_POST["email"]);
		$emailNotValid = test_email_exist($email);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    	  $emailErr = "Invalid email format<br/>"; 
		  $valid = false;
	    }
		elseif ($emailNotValid) {
			$emailErr = "Email does not exist<br/>";
			$valid = false;			
		}		
	}
	
	if($valid) {
		$_SESSION['email'] = $_POST["email"];												
		header('Location: _forgotPasswordEmail.php');
	}
}

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

function test_email_exist($data) {
	require_once 'meekrodb.2.3.class.php';
	DB::$user = 'enviroplay';
	DB::$password = 'password';
	DB::$dbName = 'e3db';
	
	$query = DB::queryFirstRow("SELECT * FROM users where email=%s", $data);
	
	if(isset($query)) {
		$value = false;
	}
	else {
		$value = true;
	}
	return $value;
}
?>

<body>
<div class="background">
  <div class="header"><?php include('header.php'); ?></div>
  <div class="clear-float"></div>
  
  	<div class="holder">
    	<div class="forgot-pass-form">
		<h3>Forgot Password</h3>
        Please enter the email address associated with your account and we will send you an email with your credentials.
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
        <p>Email: <input type="text" name="email" id="email">
        <input type="submit" value="Submit"><span class="cancel-link"><a href="main.php">Cancel</a></span></p>
        </form>
        <p><span class="error"><?php echo $emailErr ?></span></p>
        </div>
    </div>
</div>    
</body>
</html>