<?php session_start(); 
	if(isset($_SESSION['loggedin'])) {}
	else {
		header('Location: _main.php');
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>

<link rel="stylesheet" type="text/css" href="site.css">
<link rel="stylesheet" type="text/css" href="quiz.css">

<?php 
	if(isset($_POST['correct'])) {
		if($_POST['correct'] == 1) {
			$_SESSION['score'] = ++$_SESSION['score'];
		}
		if($_SESSION['questionCount'] == 10) {
			header('Location: results.php');
		}
	}
	
	if(isset($_POST['gradelevelid'])) {
		$_SESSION['gradelevel'] = $_POST['gradelevelid'];	
	}
?>
	
<?php include('getQuestion.php'); ?>

</head>

<script language="javascript" type="text/javascript">
var checkboxHeight = "50";
var radioHeight = "34";
var selectWidth = "190";


/* No need to change anything after this */


document.write('<style type="text/css">input.styled { display: none; } select.styled { position: relative; width: ' + selectWidth + 'px; opacity: 0; filter: alpha(opacity=0); z-index: 5; } .disabled { opacity: 0.5; filter: alpha(opacity=50); }</style>');

var Custom = {
	init: function() {
		var inputs = document.getElementsByTagName("input"), span = Array(), textnode, option, active;
		for(a = 0; a < inputs.length; a++) {
			if((inputs[a].type == "checkbox" || inputs[a].type == "radio") && inputs[a].className.indexOf("styled") > -1) {
				span[a] = document.createElement("span");
				span[a].className = inputs[a].type;

				if(inputs[a].checked == true) {
					if(inputs[a].type == "checkbox") {
						position = "0 -" + (checkboxHeight*2) + "px";
						span[a].style.backgroundPosition = position;
					} else {
						position = "0 -" + (radioHeight*2) + "px";
						span[a].style.backgroundPosition = position;
					}
				}
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				inputs[a].onchange = Custom.clear;
				if(!inputs[a].getAttribute("disabled")) {
					span[a].onmousedown = Custom.pushed;
					span[a].onmouseup = Custom.check;
				} else {
					span[a].className = span[a].className += " disabled";
				}
			}
		}
		inputs = document.getElementsByTagName("select");
		for(a = 0; a < inputs.length; a++) {
			if(inputs[a].className.indexOf("styled") > -1) {
				option = inputs[a].getElementsByTagName("option");
				active = option[0].childNodes[0].nodeValue;
				textnode = document.createTextNode(active);
				for(b = 0; b < option.length; b++) {
					if(option[b].selected == true) {
						textnode = document.createTextNode(option[b].childNodes[0].nodeValue);
					}
				}
				span[a] = document.createElement("span");
				span[a].className = "select";
				span[a].id = "select" + inputs[a].name;
				span[a].appendChild(textnode);
				inputs[a].parentNode.insertBefore(span[a], inputs[a]);
				if(!inputs[a].getAttribute("disabled")) {
					inputs[a].onchange = Custom.choose;
				} else {
					inputs[a].previousSibling.className = inputs[a].previousSibling.className += " disabled";
				}
			}
		}
		//document.onmouseup = Custom.clear;
	},
	pushed: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight*3 + "px";
		} else if(element.checked == true && element.type == "radio") {
			this.style.backgroundPosition = "0 -" + radioHeight*3 + "px";
		} else if(element.checked != true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 -" + checkboxHeight + "px";
		} else {
			this.style.backgroundPosition = "0 -" + radioHeight + "px";
		}
	},
	check: function() {
		element = this.nextSibling;
		if(element.checked == true && element.type == "checkbox") {
			this.style.backgroundPosition = "0 0";
			element.checked = false;
		} else {
			if(element.type == "checkbox") {
				this.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			} else {
				this.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
				group = this.nextSibling.name;
				inputs = document.getElementsByTagName("input");
				for(a = 0; a < inputs.length; a++) {
					if(inputs[a].name == group && inputs[a] != this.nextSibling) {
						inputs[a].previousSibling.style.backgroundPosition = "0 0";
					}
				}
			}
			element.checked = true;
		}
	},
	clear: function() {
		inputs = document.getElementsByTagName("input");
		for(var b = 0; b < inputs.length; b++) {
			if(inputs[b].type == "checkbox" && inputs[b].checked == true && inputs[b].className.indexOf("styled") > -1) {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + checkboxHeight*2 + "px";
			} else if(inputs[b].type == "checkbox" && inputs[b].className.indexOf("styled") > -1) {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			} else if(inputs[b].type == "radio" && inputs[b].checked == true && inputs[b].className.indexOf("styled") > -1) {
				inputs[b].previousSibling.style.backgroundPosition = "0 -" + radioHeight*2 + "px";
			} else if(inputs[b].type == "radio" && inputs[b].className.indexOf("styled") > -1) {
				inputs[b].previousSibling.style.backgroundPosition = "0 0";
			}
		}
	},
	choose: function() {
		option = this.getElementsByTagName("option");
		for(d = 0; d < option.length; d++) {
			if(option[d].selected == true) {
				document.getElementById("select" + this.name).childNodes[0].nodeValue = option[d].childNodes[0].nodeValue;
			}
		}
	}
}
window.onload = Custom.init;
</script>

<body>

<div class="background">
    <div class="quiz-header"></div>
    <div class="clear-float"></div>
    <div class="quiz-score-box">
    	<div class="score-holder">
			<div class="score-title"><div class="score-title-text">Score</div></div>
            <div class="score-box"><div class="score-text"><?php echo $_SESSION['score'] ?>/<?php echo $_SESSION['questionCount'] -1; ?></div></div>
        </div>
        
    </div>
	<div class="question-box">
        <div class="question-holder">			
			<div class="questions">
            	<div class="question-bubble"><div class="question-bubble-text"><?php echo $_SESSION['questionCount']; ?></div></div>
                <div class="question-text"><?php echo $question; ?></div>
            </div>
            <div class="answers">
	
					<input type="radio" name="ans" value="0" id="ans0" class="styled"> <div class="answer"><?php echo $answers[0]; ?></div>
   					<input type="radio" name="ans" value="1" id="ans1" class="styled"> <div class="answer"><?php echo $answers[1]; ?></div>
					<input type="radio" name="ans" value="2" id="ans2" class="styled"> <div class="answer"><?php echo $answers[2]; ?></div>
					<input type="radio" name="ans" value="3" id="ans3" class="styled"> <div class="answer"><?php echo $answers[3]; ?></div>
            </div>
				<input type="button" value="Submit" name="submit" id="submit" onClick="checkAnswer();">

		</div>
     </div>
    <div class="details-box">
    	<div id="correct-text" class="hidden">
        	<h3 class="green">You're Right!</h3>
            <span class="smaller-text"><?php echo $answerInfo; ?></span>
            <br/><br/>
            <form name="next-correct" method="post" action="quiz.php">
				<input type="text" name="correct" id="correct" value="1" hidden="true">
            	<input type="submit" value="Next" name="next">
            </form>
        </div>
        <div id="incorrect-text" class="hidden">
        	<h3 class="red">Wrong!</h3>
            <span class="smaller-text"><?php echo $answerInfo; ?></span>
            <br/><br/>
            <form name="next-correct" method="post" action="quiz.php">
				<input type="text" name="correct" id="correct" value="0" hidden="true">
            	<input type="submit" value="Next" name="next">
            </form>
        </div>
    </div>
    <div class="clear-float"></div>
    <div class="footer"><?php include('footer.php'); ?></div>
</div>

<script language="javascript" type="text/javascript">
function checkAnswer(){
	 var correctAnswer = "<?php echo $correctAnswer; ?>";
	 
	 if (document.getElementById('ans0').checked) {
  		var answer = document.getElementById('ans0').value;
	 }
	 else if (document.getElementById('ans1').checked) {
  		var answer = document.getElementById('ans1').value;
	 }
	 else if (document.getElementById('ans2').checked) {
  		var answer = document.getElementById('ans2').value;
	 }
	 else if (document.getElementById('ans3').checked) {
  		var answer = document.getElementById('ans3').value;
	 }
	 
	 if (answer == correctAnswer) {
	 	var id="correct-text";
		var myClassName="hidden"; //must keep a space before class name
		var d;
		d=document.getElementById(id);
		d.className=d.className.replace(myClassName,"");
	 }
	 else {
	 	var id="incorrect-text";
		var myClassName="hidden"; //must keep a space before class name
		var d;
		d=document.getElementById(id);
		d.className=d.className.replace(myClassName,"");
	 }
	 
	 var r1 = document.getElementById('submit');
	 r1.setAttribute("hidden", "true");
}
//-->
</script>

</body>
</html>