<?php
	session_start();
	require_once 'meekrodb.2.3.class.php';
	DB::$user = 'enviroplay';
	DB::$password = 'password';
	DB::$dbName = 'e3db';
	
	$username = $_SESSION['username'];
	$password = $_SESSION['password'];
	
	$fname = $_SESSION['fname'];
	$lname = $_SESSION['lname'];
	$city = $_SESSION['city'];
	$state = $_SESSION['state'];
	$email = $_SESSION['email'];
	
	$hash = password_hash($password, PASSWORD_DEFAULT);
	
	DB::insert('users', array('username' => $username, 'password' => $password, 'first_name' => $fname, 'last_name' => $lname, 'city' => $city, 'state' => $state, 'email' => $email));
	
	$_SESSION['message'] = 'Account Created!';
	header('Location: _main.php');
?>