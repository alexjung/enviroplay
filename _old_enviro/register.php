<?php session_start(); ?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>

<link rel="stylesheet" type="text/css" href="site.css">
<link rel="stylesheet" type="text/css" href="register.css">

</head>

<?php
$username = $password1 = $password2 = $state = $fname = $lname = $city = $email = "";
$usernameErr = $passwordErr = $passwordMatchErr = $stateErr = $emailErr = $fnameErr = "";
$usernameTaken = "";
$valid = true;

if ($_SERVER["REQUEST_METHOD"] == "POST") {
	$lname = $_POST['lname'];
	$city = $_POST['city'];	
	
	if (empty($_POST["username"])) {	
		$usernameErr = "Username is required<br/>";
		$valid = false;
	} else {
		$username = test_input($_POST["username"]);
		$usernameTaken = test_dup_username($username);
		if (isset($usernameTaken)) {
			$usernameErr = "Username is already taken<br/>";
			$valid = false;			
		}		
	}
	
	if (empty($_POST["password1"])) {	
		$passwordErr = "Password is required<br/>";
		$valid = false;		
	} else {
		$password1 = test_input($_POST["password1"]);
		$password2 = test_input($_POST["password2"]);
		if($password1 != $password2) {
			$passwordMatchErr = "Passwords must match<br/>";
			$valid = false;
		}
	}
	
	if (empty($_POST["email"])) {	
		$emailErr = "Email is required<br/>";
		$valid = false;		
	} else {
		$email = test_input($_POST["email"]);
	    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    	  $emailErr = "Invalid email format<br/>"; 
	    }
		else {
			$emailTaken = test_dup_email($email);
			if (isset($emailTaken)) {
				$emailErr = "Email is already taken<br/>";
				$valid = false;			
			}	
		}
	}
	
	if (empty($_POST["state"])) {	
		$stateErr = "State is required<br/>";
		$valid = false;		
	} else {
		$state = test_input($_POST["state"]);
	}
	
	if (empty($_POST["fname"])) {	
		$fnameErr = "First Name is required<br/>";
		$valid = false;		
	} else {
		$fname = test_input($_POST["fname"]);
	}

	
	if($valid) {
		$_SESSION['username'] = $_POST["username"];
		$_SESSION['password'] = $_POST["password1"];
		$_SESSION['fname'] = $_POST["fname"];
		$_SESSION['lname'] = $_POST["lname"];
		$_SESSION['email'] = $_POST["email"];		
		$_SESSION['city'] = $_POST["city"];
		$_SESSION['state'] = $_POST["state"];										
		header('Location: registerForm.php');
	}
}

function test_input($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
}

function test_dup_username($data) {
	require_once 'meekrodb.2.3.class.php';
	DB::$user = 'enviroplay';
	DB::$password = 'password';
	DB::$dbName = 'e3db';
	
	$query = DB::queryFirstRow("SELECT * FROM users where username=%s", $data);
	
	return $query;
}
function test_dup_email($data) {
	require_once 'meekrodb.2.3.class.php';
	DB::$user = 'enviroplay';
	DB::$password = 'password';
	DB::$dbName = 'e3db';
	
	$query = DB::queryFirstRow("SELECT * FROM users where email=%s", $data);
	
	return $query;
}
?>

<body>
<div class="background">
  <div class="header"><?php include('header.php'); ?></div>
  <div class="clear-float"></div>
  
  	<div class="register-holder">
    <div class="register-form">
		<h3>Sign Up!</h3>
		<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post">
        <div class="register-labels1">            
                <p>Username:</p>
                <p>Password:</p>
                <p>Confirm Password:</p>
                <p>First Name:</p>
                <p>Last Name:</p>
                <p>Email:</p>
        </div>
		<div class="register-inputs">            
                <p><input type="text" name="username" id="username" value="<?php echo $username;?>" maxlength="20"> *</p>
                <p><input type="password" name="password1" id="password1" value="<?php echo $password1;?>"> *</p>
                <p><input type="password" name="password2" id="password2" value="<?php echo $password2;?>"> *</p>
                <p><input type="text" name="fname" id="fname" value="<?php echo $fname;?>"> *</p>
                <p><input type="text" name="lname" id="lname" value="<?php echo $lname;?>"></p>
                <p><input type="text" name="email" id="email" value="<?php echo $email;?>"> *</p>
        </div>
        <div class="register-labels2">
                <p>City:</p>
                <p>State:</p>  
    	</div>
        <div class="register-inputs">
                <p><input type="text" name="city" id="city" value="<?php echo $city;?>"></p>
                <p><select name="state" id="state"  value="<?php echo $state;?>">
                            <option disabled selected></option>
                            <option value="MD" <?php if($state == 'MD'){echo("selected");}?>>MD</option>
                            <option value="VA" <?php if($state == 'VA'){echo("selected");}?>>VA</option>
                            <option value="PA" <?php if($state == 'PA'){echo("selected");}?>>PA</option>
                            <option value="DC" <?php if($state == 'DC'){echo("selected");}?>>DC</option>                
                   </select> *</p>  
                <input type="submit" value="Sign up"><span class="cancel-link"><a href="main.php">Cancel</a></span>
                <p><span class="error"><?php echo $usernameErr;?></span>
                <span class="error"><?php echo $passwordErr;?></span>
                <span class="error"><?php echo $passwordMatchErr;?></span>
                <span class="error"><?php echo $fnameErr;?></span>
                <span class="error"><?php echo $emailErr;?></span>                            
                <span class="error"><?php echo $stateErr;?></span></p>               
    	</div>
        </form>
	</div>
    </div>
</div>
</body>
</html>