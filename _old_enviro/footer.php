<?php

	if(isset($_SESSION['loggedin'])) {
		echo '<div class="username-box"><div class="username-text">' .$_SESSION["username"] . '</div></div>';
		
		$host = $_SERVER['REQUEST_URI'];
			
		if($host == '/enviroplay/quiz.php') 
		{
			echo '
				<div class="footer-quit">
					<form method="post" action="results.php">
						<input type="submit" value="Quit Quiz">
					</form>
				</div>';
		}
		elseif($host == '/enviroplay/_main.php')
		{
			echo '
				<div class="footer-quit">
					<form method="post" action="logout.php">
						<input type="submit" value="Logout">
					</form>
				</div>';
		}

	}
	
?>