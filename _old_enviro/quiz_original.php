<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<title>Untitled Document</title>

<link rel="stylesheet" type="text/css" href="site.css">
<link rel="stylesheet" type="text/css" href="quiz.css">

</head>
<?php include('getQuestion.php'); ?>
<body>

<div class="background">
	<div class="quiz-header">Quiz Header</div>
    <div class="clear-float"></div>
	
	<div class="question-box">
    	<a href="main.php">Main</a>
        <div class="question-holder">
			
			<div class="questions"><?php echo $question; ?></div>
            <div class="answers">
	
					<input type="radio" name="ans" value="0" id="ans0"> <?php echo $answers[0]; ?>
                    <br/>
   					<input type="radio" name="ans" value="1" id="ans1"> <?php echo $answers[1]; ?>
                    <br/>
					<input type="radio" name="ans" value="2" id="ans2"> <?php echo $answers[2]; ?>
                    <br/>        
					<input type="radio" name="ans" value="3" id="ans3"> <?php echo $answers[3]; ?>
                    <br/>
            </div>
				<input type="button" value="Submit" name="submit" onClick="checkAnswer();">

		</div>
     </div>
    <div class="details-box">
    	<div id="correct-text" class="hidden">
        	<h3 class="green">You're Right!</h3>
            <span class="smaller-text"><?php echo $answerInfo; ?></span>
            <br/>
            <input type="button" value="Next" name="next" onClick="window.location.reload();">
        </div>
        <div id="incorrect-text" class="hidden">
        	<h3 class="red">Wrong!</h3>
            <span class="smaller-text"><?php echo $answerInfo; ?></span>
            <input type="button" value="Next" name="next" onClick="window.location.reload();">
        </div>
    </div>
</div>

<script language="javascript" type="text/javascript">
<!-- 
//Browser Support Code
function ajaxFunction(){
	var ajaxRequest;  // The variable that makes Ajax possible!
	
	try{
		// Opera 8.0+, Firefox, Safari
		ajaxRequest = new XMLHttpRequest();
	} catch (e){
		// Internet Explorer Browsers
		try{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		} catch (e) {
			try{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			} catch (e){
				// Something went wrong
				alert("Your browser broke!");
				return false;
			}
		}
	}
	// Create a function that will receive data sent from the server
	ajaxRequest.onreadystatechange = function(){
		if(ajaxRequest.readyState == 4){
			
			var id="details-text";
			var myClassName="hidden"; //must keep a space before class name
			var d;
			d=document.getElementById(id);
			d.className=d.className.replace(myClassName,"");
		}
	}
	ajaxRequest.open("GET", "verifyAnswer.php", true);
	ajaxRequest.send(null); 
}

function checkAnswer(){
	 var correctAnswer = "<?php echo $correctAnswer; ?>";
	 
	 if (document.getElementById('ans0').checked) {
  		var answer = document.getElementById('ans0').value;
	 }
	 else if (document.getElementById('ans1').checked) {
  		var answer = document.getElementById('ans1').value;
	 }
	 else if (document.getElementById('ans2').checked) {
  		var answer = document.getElementById('ans2').value;
	 }
	 else if (document.getElementById('ans3').checked) {
  		var answer = document.getElementById('ans3').value;
	 }
	 
	 if (answer == correctAnswer) {
	 	var id="correct-text";
		var myClassName="hidden"; //must keep a space before class name
		var d;
		d=document.getElementById(id);
		d.className=d.className.replace(myClassName,"");
	 }
	 else {
	 	var id="incorrect-text";
		var myClassName="hidden"; //must keep a space before class name
		var d;
		d=document.getElementById(id);
		d.className=d.className.replace(myClassName,"");
	 }
}
//-->
</script>

</body>
</html>