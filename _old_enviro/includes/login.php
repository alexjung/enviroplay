<?php
	session_start();
	ob_start();
	
	require_once 'meekrodb.2.3.class.php';
	DB::$user = 'enviroplay';
	DB::$password = 'password';
	DB::$dbName = 'e3db';
	
	$username = $_POST['myusername'];
	$password = $_POST['mypassword'];
	
	if(empty($username))
	{
		$_SESSION['loginfailed'] = 1;
		header('Location: _main.php');
	}
	elseif(empty($password))
	{
		$_SESSION['loginfailed'] = 1;
		header('Location: _main.php');
	}
	else {
		$result = DB::queryFirstRow("SELECT * FROM users WHERE username = %s", $username);
		
		$hash = $result['password'];
		
		if ($password == $hash) {
			$_SESSION['loggedin'] = 1;
			$_SESSION['username'] = $result['username'];
			$_SESSION['fname'] = $result['first_name'];
			$_SESSION['questionIDs'] = array(0);
			$_SESSION['questionCount'] = '0';
			$_SESSION['score'] = '0';
			header('Location: _main.php');
		}
		else {
			$_SESSION['loginfailed'] = 1;
			header('Location: _main.php');
		}
	}
?>