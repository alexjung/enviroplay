<?php
	
	require_once 'meekrodb.2.3.class.php';
	DB::$user = 'enviroplay';
	DB::$password = 'password';
	DB::$dbName = 'e3db';
			
	$questionDB = DB::queryFirstRow( 'SELECT * FROM question WHERE question_id >= (SELECT FLOOR( MAX(question_id) * RAND()) FROM question WHERE level_id = %i ) AND level_id = %i AND question_id NOT IN %li ORDER BY question_id LIMIT 1',$_SESSION['gradelevel'],$_SESSION['gradelevel'],$_SESSION['questionIDs']);
	
	$questionID = $questionDB['question_id'];
	$question = $questionDB['question'];
	$answerInfo = $questionDB['right_wrong_desc'];
	
	$_SESSION['questionCount'] = ++$_SESSION['questionCount'];
	
	array_push($_SESSION['questionIDs'],$questionID);
		
	$answersDB = DB::query('SELECT * FROM answer WHERE question_id = %i', $questionID);
	
	$answers = array();	
	foreach ($answersDB as $row) {
		array_push($answers, $row['answer']);
		if($row['correct'] == 'Y') {
			$correctAnswer = max(array_keys($answers));
		}
	}
	

?>