<?php session_start(); 
	if(isset($_SESSION['loggedin'])) {}
	else {
		header('Location: _main.php');
	}
?>
<!doctype html>
<html>
<head>
<meta charset="UTF-8">

<title>Results</title>

<link rel="stylesheet" type="text/css" href="site.css">
<link rel="stylesheet" type="text/css" href="results.css">

</head>

<body>

<div class="background">
  <div class="header"><?php include('header.php'); ?></div>
  <div class="clear-float"></div>
  
    <div class="holder">
        <div class="results-info">
        	<?php echo $_SESSION['score'] ?>/<?php echo $_SESSION['questionCount'] - 1; ?>
            <br/><br/><br/>
            <form action="main.php" method="post">
	            <input type="submit" value="ALL DONE!">
			</form>
            <?php 
				unset($_SESSION['questionIDs']);
				unset($_SESSION['questionCount']);
				unset($_SESSION['score']);
				$_SESSION['questionIDs'] = array(0);
				$_SESSION['questionCount'] = '0';
				$_SESSION['score'] = '0';
			?>
        </div>
        
    </div>
    <div class="clear-float"></div>
    <div class="footer"><?php include('footer.php'); ?></div>
</div>    

</body>
</html>