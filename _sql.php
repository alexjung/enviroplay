<?php
require_once 'includes/utilityFunctions.php';
ob_start();

$cnt = 29;

DB::startTransaction();

try {
    /*$q = DB::query("SELECT * FROM question ORDER BY question_id");
    $t = DB::query("SELECT topic_area_id FROM ref_topic_area ORDER BY topic_area_id");
    $g = DB::query("SELECT level_id FROM ref_level ORDER BY level_id");

    foreach ($t AS $t_idx => $t_key){
        foreach ($g AS $g_idx => $g_key){
            $doInsert = true;
            if ($g[$g_idx]['level_id'] == 1 && $t[$t_idx]['topic_area_id'] == 1){
                $doInsert = false;
            }
            if ($doInsert){
                foreach ($q as $q_idx => $q_key){
                    $newQ = $q[$q_idx];
                    $cnt++;

                    $insert = DB::query("INSERT INTO question (question_id, level_id, topic_area_id, question_type_id, question, right_wrong_desc, source, reminder, tf_ans) VALUES (%i, %i, %i, %i, %s, %s, %s, %s, %s)", $cnt, $g[$g_idx]['level_id'], $t[$t_idx]['topic_area_id'], $newQ['question_type_id'], $newQ['question'], $newQ['right_wrong_desc'], $newQ['source'], $newQ['reminder'], $newQ['tf_ans']);

                    $a = DB::query("SELECT * FROM answer WHERE question_id = %i", $newQ['question_id']);

                    foreach ($a as $a_idx => $a_key){
                        $insert = DB::query("INSERT INTO answer (answer_id, question_id, answer, correct) VALUES (NULL, %i, %s, %s)", $cnt, $a[$a_idx]['answer'], $a[$a_idx]['correct']);
                    }
                }
            }
        }
    }*/

    $q = DB::query("SELECT MAX(question_id) AS num FROM question");
    $cnt = $q[0]['num'];

    $q = DB::query("SELECT * FROM question WHERE level_id = 1 AND question_type_id = 1 ORDER BY question_id");
    foreach ($q AS $q_idx => $q_key) {
        $newQ = $q[$q_idx];
        $cnt++;

        $insert = DB::query("INSERT INTO question (question_id, level_id, topic_area_id, question_type_id, question, right_wrong_desc, source, reminder, tf_ans) VALUES (%i, %i, %i, %i, %s, %s, %s, %s, %s)", $cnt, $newQ['level_id'], $newQ['topic_area_id'], 3, $newQ['question'], $newQ['right_wrong_desc'], $newQ['source'], $newQ['reminder'], $newQ['tf_ans']);

        $a = DB::query("SELECT * FROM answer WHERE question_id = %i", $newQ['question_id']);
        $answerCnt = 1;
        foreach ($a as $a_idx => $a_key) {
            $insert = DB::query("INSERT INTO answer (answer_id, question_id, answer, correct) VALUES (NULL, %i, %s, %s)", $cnt, $a[$a_idx]['answer'], $answerCnt);
            $answerCnt++;
        }
    }
    DB::commit();
}
catch (Exception $e){
    DB::rollback();
}
?>