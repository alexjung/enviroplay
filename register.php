<?php
require_once 'includes/utilityFunctions.php';

// set up referrer since users can come in via multiple places (index & playerStart.php)
if (strpos($_SERVER['HTTP_REFERER'], $_SERVER['PHP_SELF']) === false) {
    $_SESSION['tmpReferer'] = $_SERVER['HTTP_REFERER'];
}

// set up a list of form fields to be processed
$formFields = [
    'username' => '',
    'password1' => '',
    'password2' => '',
    'fname' => '',
    'lname' => '',
    'email' => '',
    'city' => '',
    'state' => ''
];

$validStates = ['MD','VA','PA','DC'];

// if user posted the form
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $_SESSION['systemMessage'] =[];
    $errorExists = false;

    // validate each form field
    foreach($formFields as $fld => $fldVal){
        if (!isset($_POST[$fld])){
            $_POST[$fld] = "";
        }
        else {
            $_POST[$fld] = trim($_POST[$fld]);
        }
        // if it's empty, set error message
        if ($_POST[$fld] == ''){
            $missingFieldErr = ['danger', '<strong>Error:</strong> All fields are requried.'];
            $errorExists = true;
            if (!in_array($missingFieldErr, $_SESSION['systemMessage'])){
                array_push($_SESSION['systemMessage'], $missingFieldErr);
            }
        }
        // otherwise default the value
        else {
            switch($fld){
                case "lname":
                case "city":
                    $formFields[$fld] = $_POST[$fld];
                    break;

                default:
                    $formFields[$fld] = test_input($_POST[$fld]);
                    break;
            }
        }
    }

    // check if username exists
    $dupExists = test_dup('username', $formFields['username']);
    if (isset($dupExists)){
        $errorExists = true;
        array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Username is already taken.']);
    }
    // check passwords
    if ($formFields['password1'] != $formFields['password2']){
        $errorExists = true;
        array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Passwords must match.']);
    }
    // check email
    if ($formFields['email'] != '' && !filter_var($formFields['email'], FILTER_VALIDATE_EMAIL)) {
        $errorExists = true;
        array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Invalid email format.']);
    }
    else {
        $dupExists = test_dup('email', $formFields['email']);
        if (isset($dupExists)){
            $errorExists = true;
            array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Email is already taken.']);
        }
    }
    // check state
    if ($formFields['state'] != ''){
        if (!in_array($formFields['state'], $validStates)){
            $errorExists = true;
            array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Please select a valid state.']);
        }
    }

    // error exists, kick back to form with messaging
    if ($errorExists){
        // nothing needed right now; display form with error messages
    }
    // send to includes/registerForm to save values to DB
    else {
        if (!$isUserLoggedIn) {
            $_SESSION['user'] = [];
        }
        $tmp = [
            "username" => $_POST['username'],
            "password" => $_POST['password1'],
            "fname" => $_POST['fname'],
            "lname" => $_POST['lname'],
            "email" => $_POST['email'],
            "city" => $_POST['city'],
            "state" => $_POST['state'],
            "questionIDs" => [],
            "questionCount" => 0,
            "score" => 0,
            "admin" => 0
        ];
        array_push($_SESSION['user'], $tmp);

        header('Location: includes/registerForm.php');
    }

}

$pageTitle = "Sign Up!";

require_once 'includes/template/header.php';
?>
<div class="container main-container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>

    <div class="well well-lg clearfix">
		<div class="row">
            <div class="col-md-12">
                <div class="well well-sm req-intro"><strong><i class="glyphicon glyphicon-ok form-control-feedback"></i> Required Field</strong></div>
            </div>

    		<form role="form" action="<?php echo htmlentities($_SERVER["PHP_SELF"]);?>" method="post" class="form-horizontal">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="username" class="col-xs-4">Username:</label>
                        <div class="input-group col-xs-8">
                            <input type="text" class="form-control" name="username" id="username" value="<?php echo $formFields['username']; ?>" placeholder="Username" maxlength="20" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password1" class="col-xs-4">Password:</label>
                        <div class="input-group col-xs-8">
                            <input type="password" class="form-control" name="password1" id="password1" value="<?php echo $formFields['password1'];?>" placeholder="Password" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password2" class="col-xs-4">Confirm Password:</label>
                        <div class="input-group col-xs-8">
                            <input type="password" class="form-control" name="password2" id="password2" value="<?php echo $formFields['password2'];?>" placeholder="Confirm Password" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="fname" class="col-xs-4">First Name:</label>
                        <div class="input-group col-xs-8">
                            <input type="text" class="form-control" name="fname" id="fname" value="<?php echo $formFields['fname']; ?>" placeholder="First Name" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="lname" class="col-xs-4">Last Name:</label>
                        <div class="input-group col-xs-8">
                            <input type="text" class="form-control" name="lname" id="lname" value="<?php echo $formFields['lname']; ?>" placeholder="Last Name" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-xs-4">Email:</label>
                        <div class="input-group col-xs-8">
                            <input type="text" class="form-control" name="email" id="email" value="<?php echo $formFields['email']; ?>" placeholder="Email" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                </div>

                <div class="col-md-6">
                    <div class="form-group">
                        <label for="city" class="col-xs-4">City:</label>
                        <div class="input-group col-xs-8">
                            <input type="text" class="form-control" name="city" id="city" value="<?php echo $formFields['city'];?>" placeholder="City" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="state" class="col-xs-4">State:</label>
                        <div class="input-group col-xs-8">
                            <select class="form-control" name="state" id="state">
                                <option disabled<?php echo ($formFields['state'] == '') ? ' selected' : ''; ?>>State</option>
<?php
foreach ($validStates AS $state):
    echo '                                <option value="' . $state . '"' . (($formFields['state'] == $state) ? " selected" : "") . ">" . $state . "</option>";
endforeach;
?>
                            </select>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="col-xs-8 col-xs-offset-4">
                        <button type="submit" class="btn btn-default">Sign up</button> <a href="index.php"><button type="button" class="btn btn-lg btn-link">Cancel</button></a>
                    </div>

                </div>
            </form>
        </div>
    </div>
</div>

<?php
require_once 'includes/template/footer.php';
?>
