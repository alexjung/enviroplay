<?php
require_once 'includes/utilityFunctions.php';

// page-specifc logic
$pageTitle = "Contact Us";
$formFields = [
    'contactName' => '',
    'contactEmail' => '',
    'contactMessage' => '',
    'contactCheck' => ''
];
$spamCheckMath = "4 + 3";

// check if user submitted the form
if ($_SERVER["REQUEST_METHOD"] == "POST"){
    $errorExists = false;

    // check if any fields are empty
    foreach($formFields as $fld => $fldVal){
        if ($_POST[$fld] == ''){
            $errorExists = true;
            $_SESSION['systemMessage'] = [['danger', '<strong>Error:</strong> All fields are requried.']];
        }
        else {
            $formFields[$fld] = $_POST[$fld];
        }
    }
    // check if a valid email was entered
    if ($_POST['contactEmail'] != '' && !filter_var($_POST['contactEmail'], FILTER_VALIDATE_EMAIL)){
        $errorExists = true;
        array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Please enter a valid email address.']);
    }
    // check if spam checker is passed; yes, going to use an eval
    if ($_POST['contactCheck'] != '' && !(eval('return ' . $spamCheckMath . ';') == $_POST['contactCheck'])){
        $errorExists = true;
        array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Oops, are you a spam bot :)? Please enter a valid response to the Simple Spam Checker.']);
    }

    // if no validation error exists, send email
    if (!($errorExists)) {
        $to = "alex_jung@sra.com";
        $subject = "EnviroPlay";

        $headers   = array();
        $headers[] = "MIME-Version: 1.0";
        $headers[] = "Content-type: text/plain; charset=iso-8859-1";
        $headers[] = "From: $contactName <$contactEmail>";
        //$headers[] = "Bcc: Alex Jung <alex_jung@sra.com>";
        //$headers[] = "Reply-To: Recipient Name <receiver@domain3.com>";
        $headers[] = "Subject: $subject";
        $headers[] = "X-Mailer: PHP/".phpversion();

        $mailSent = mail($to, $subject, $_POST['contactMessage'], implode("\r\n", $headers));

        // check if sending mail was successful
        if ($mailSent){
            $_SESSION['systemMessage'] = [['success', "<span class='glyphicon glyphicon-send'></span> <strong>Success:</strong> Message Sent." ]];
        }
        else {
            $_SESSION['systemMessage'] = [['danger', "<strong>Error:</strong> I'm sorry, there was an error sending the email. Please try again." ]];
        }

    }
}

require_once 'includes/template/header.php';
?>
<div class="container main-container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>

    <div class="well well-lg clearfix">
        <div class="row">
            <div class="col-md-12">
                <div class="well well-sm req-intro"><strong><i class="glyphicon glyphicon-ok form-control-feedback"></i> Required Field</strong></div>
            </div>

            <form role="form" action="contact.php" method="post">
                <div class="col-md-7 clearfix">
                    <div class="form-group">
                        <label for="contactName">Your Name</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="contactName" id="contactName" value="<?php echo $formFields['contactName']; ?>" placeholder="Enter Name" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="contactEmail">Your Email</label>
                        <div class="input-group">
                            <input type="email" class="form-control" id="contactEmail" name="contactEmail" value="<?php echo $formFields['contactEmail']; ?>" placeholder="Enter Email" required>
                            <span class="input-group-addon" style=""><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="contactMessage">Message</label>
                        <div class="input-group"
                            >
                            <textarea name="contactMessage" id="contactMessage" class="form-control" rows="5" placeholder="Enter Message" required><?php echo $formFields['contactMessage']; ?></textarea>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="contactCheck">What is <?php echo $spamCheckMath; ?>? (Simple Spam Checker)</label>
                        <div class="input-group">
                            <input type="text" class="form-control" name="contactCheck" id="contactCheck" value="<?php echo $formFields['contactCheck']; ?>" placeholder="<?php echo $spamCheckMath; ?> =" required>
                            <span class="input-group-addon"><i class="glyphicon glyphicon-ok form-control-feedback"></i></span>
                        </div>
                    </div>
                    <input type="submit" name="submit" id="submit" value="Submit" class="btn btn-default">
                    <a href="index.php"><button type="button" class="btn btn-lg btn-link">Cancel</button></a>

                </div>
            </form>

            <hr class="featurette-divider hidden-lg">

            <div class="col-md-4 col-sm-push-1">
                <address>
                    <h3 class="first">Office Location</h3>
                    <p class="lead">
                        <a href="https://www.google.com/maps/place/Metropolitan+Bldg,+3434+Washington+Blvd,+Arlington,+VA+22201/@38.8856508,-77.1017925,17z/data=!3m1!4b1!4m2!3m1!1s0x89b7b682633612c1:0x2ef5cf32ed9976f3">
                            3434 Washington Blvd.<br>
                            2nd Floor AC 2189<br>
                            Arlington, VA 22201
                        </a><br>
                        Phone 703.284.6059<br>
                        Fax 540.668.7711
                    </p>
                </address>
            </div>
        </div>
    </div>
</div>

<?php
$pageJs = <<<ENDOFSCRIPT
<script type="text/javascript" src="js/jquery.autosize.min.js"></script>
<script language="javascript" type="text/javascript">
$('textarea').autosize();
</script>
ENDOFSCRIPT;

require_once 'includes/template/footer.php';
?>
