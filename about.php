<?php
require_once 'includes/utilityFunctions.php';

$pageTitle = "About EnviroPlay";

require_once 'includes/template/header.php';
?>

<div class="container" role="main">
    <h1 class="first"><?php echo $pageTitle; ?></h1>

    <div class="well well-lg clearfix">
        <p>Lorem ipsum dolor sit amet, lectus sit, lacinia malesuada ullamcorper lacus vitae aliquam, vestibulum interdum, mollis suscipit ut consequat. Urna ac, enim non integer tincidunt elit vitae, nulla donec. Laoreet viverra, eu ultrices ipsum congue, maecenas pede. Ad ut vestibulum lectus urna, viverra imperdiet rem quis tellus nulla et, nec wisi sed, hendrerit facilisis ullamcorper, neque ut odio nunc vel ac pretium. Pellentesque pellentesque, vitae euismod nam, inceptos lectus pellentesque omnis elit. Aliquam cum lacinia, eu justo sodales maecenas, nam mauris duis posuere, dignissim a a lorem condimentum auctor, facilisis suspendisse eros enim dolor. Neque rhoncus pretium maecenas id quis. Nec ullamcorper, justo donec urna lacus, id hendrerit sollicitudin. A a. Tincidunt volutpat pede non quam, massa odio mauris proin quam tellus, eget velit, in urna ut tellus a.</p>
    </div>
</div>

<?php
require_once 'includes/template/footer.php';
?>
