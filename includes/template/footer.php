<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xs-6" style="margin-top:1.5em;">
                <img src="<?php echo ROOT_DIR_PATH; ?>img/nat_gen_logo.png" width="152" height="47" alt="Nature Generation">
            </div>

<?php
if ($isUserLoggedIn):
?>
            <div class="col-xs-6 username">
<?php
    // if quiz, show username of current user
    if (SCRIPT_BASE_NAME == "quiz"):
?>
                <div class="username-box"><span><?php echo $_SESSION["user"][$_SESSION['userIdx']]["username"]; ?></span></div>
<?php
    // else show username of all users
    else:
?>
                <div class="username-box"><span><?php
        for ($i = 0; $i < count($_SESSION['user']); $i++):
            if ($i > 0):
                if ($i != count($_SESSION['user']) - 1):
?>
                                    ,
<?php
                else:
?>
                                    &amp;
<?php
                endif;
            endif;

            echo $_SESSION["user"][$i]["username"];
        endfor;
?></span></div>
<?php
    endif;
    // scriptBaseName located in includes/utilityFunctions
    switch(SCRIPT_BASE_NAME){
        case "index":
            echo '            <a href="includes/logout.php"><button class="btn btn-default">Logout</button></a>';
            break;

        case "quiz":
            echo '            <a href="results.php" class="submit-quiz"><button class="btn btn-default">Quit Quiz</button></a>';
            break;

        case "playerStart":
            echo '            <a href="index.php"><button class="btn btn-default">Quit Quiz</button></a>';
            break;

        case "admin":
            echo '            <a href="../includes/logout.php"><button class="btn btn-default">Logout</button></a> <a href="../index.php"><button class="btn btn-default">Home</button></a>';
            break;
            break;
    }
?>
            </div>
<?php
else:
?>
            <div class="col-xs-6 username">
<?php
    switch(SCRIPT_BASE_NAME){
        case "playerStart":
            echo '                <a href="index.php"><button class="btn btn-default">Quit Quiz</button></a>';
            break;
    }
?>
            </div>
<?php
endif;
?>
        </div>
    </div>
</footer>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="<?php echo ROOT_DIR_PATH; ?>js/bootstrap.min.js"></script>
<script src="<?php echo ROOT_DIR_PATH; ?>js/ie10-viewport-bug-workaround.min.js"></script>
<script src="<?php echo ROOT_DIR_PATH; ?>js/main.min.js"></script>
<?php
if (isset($pageJs)){
    echo $pageJs;
}
?>
</body>
</html>