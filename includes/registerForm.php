<?php
require_once 'utilityFunctions.php';

$user = $_SESSION['user'][count($_SESSION['user']) - 1];

$username = $user['username'];
$password = $user['password'];

$fname = $user['fname'];
$lname = $user['lname'];
$city = $user['city'];
$state = $user['state'];
$email = $user['email'];
$admin = $user['admin'];

$hash = password_hash($password, PASSWORD_DEFAULT);

/* one last sanity check */
$query = DB::queryFirstRow("SELECT * FROM users WHERE username=%s AND email=%s", $username, $email);

if (is_null($query)){
    DB::insert('users', array('username' => $username, 'password' => $password, 'first_name' => $fname, 'last_name' => $lname, 'city' => $city, 'state' => $state, 'email' => $email));

    $_SESSION['systemMessage'] = [['success', '<strong>Success:</strong> Your account was successfully created.']];
}
else {
    $_SESSION['systemMessage'] = [['warning', '<strong>Warning:</strong> An account was already created.']];
}

$tmpReferer = '../index.php';
if (isset($_SESSION['tmpReferer'])){
    $tmpReferer = $_SESSION['tmpReferer'];
    unset($_SESSION['tmpReferer']);
}
header('Location: ' . $tmpReferer);
?>