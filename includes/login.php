<?php
require_once 'utilityFunctions.php';
ob_start();

$username = $_POST['myusername'];
$password = $_POST['mypassword'];

if(empty($username))
{
    $_SESSION['loginfailed'] = 1;
    $_SESSION['systemMessage'] = [['danger', '<strong>Error:</strong> Please enter your Username.']];
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
}
elseif(empty($password))
{
    $_SESSION['loginfailed'] = 1;
    $_SESSION['systemMessage'] = [['danger', 'Please enter your Password.']];
    header('Location: ' . $_SERVER['HTTP_REFERER']);
    exit;
}
else {
    $result = DB::queryFirstRow("SELECT * FROM users WHERE username = %s", $username);

    $hash = $result['password'];

    if ($password == $hash) {
        // check if user is already logged in
        for ($i = 0; $i < count($_SESSION['user']); $i++) {
            if ($username == $_SESSION['user'][$i]['username']){
                $_SESSION['loginfailed'] = 1;
                $_SESSION['systemMessage'] = [['danger', 'A user with the username and password has already logged in. Please try again with a different user']];
                header('Location: ' . $_SERVER['HTTP_REFERER']);
                exit;
            }
        }

        // else log the user in
        $_SESSION['loggedin'] = 1;
        $_SESSION['username'] = $result['username'];
        $_SESSION['fname'] = $result['first_name'];
        $_SESSION['questionIDs'] = array(0);
        $_SESSION['questionCount'] = '0';
        $_SESSION['score'] = '0';
        $_SESSION['admin'] = $result['admin'];

        if (!$isUserLoggedIn) {
            $_SESSION['user'] = [];
        }
        $tmp = [
            "admin" => $result['admin'],
            "city" => $result['city'],
            "email" => $result['email'],
            "fname" => $result['first_name'],
            "lname" => $result['last_name'],
            "password" => $result['password'],
            "questionCount" => 0,
            "questionIDs" => [],
            "questionIncorrect" => [],
            "score" => 0,
            "state" => $result['state'],
            "timer" => [],
            "username" => $result['username']
        ];
        array_push($_SESSION['user'], $tmp);

        $_SESSION['systemMessage'] = [['success', $result['first_name'] . " (" . $result['username'] . ") has been successfully logged in."]];
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }
    else {
        $_SESSION['loginfailed'] = 1;
        $_SESSION['systemMessage'] = [['danger', '<strong>Error:</strong> There was no account for the username and password provided. Would you like to <a href="register.php" class="alert-link">create an account</a>?']];
        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }
}
?>