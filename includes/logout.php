<?php
session_start();
session_unset();
session_destroy();

session_start();
$_SESSION['systemMessage'] = [['success', 'You have been logged out successfully.']];
header('Location: ../index.php');
?>