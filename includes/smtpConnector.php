<?php
require_once 'PHPMailerAutoload.php';

$mail = new PHPMailer;

//$mail->SMTPDebug = 3;                               // Enable verbose debug output

$mail->isSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.gmail.com';                       // Specify main and backup SMTP servers
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'justinrkent@gmail.com';            // SMTP username
$mail->Password = 'W1nd0w524!';                       // SMTP password
$mail->SMTPSecure = 'ssl';                            // Enable TLS encryption, `ssl` also accepted
$mail->Port = 465;                                    // TCP port to connect to

$mail->From = 'justinrkent@gmail.com';
$mail->FromName = 'EnviroPlay';
$mail->addAddress($emailTo);     // Add a recipient

$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = $emailSubject;
$mail->Body    = $emailBody;
$mail->AltBody = $emailAltBody;

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;

    $_SESSION['systemMessage'] = [['danger', '<strong>Error:</strong> Message could not be sent.']];
} else {
    echo 'Message has been sent';

    $_SESSION['systemMessage'] = [['success', '<strong>Success:</strong> Message has been sent.']];
}
?>