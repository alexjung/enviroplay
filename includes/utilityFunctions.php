<?php
// start session
if (session_id() == "") {
    session_start();
}

// set up a var to hold the directory path to the root (used mainly for including css/js files in the template
define('ROOT_DIR_PATH', str_replace('/admin/', '/', dirname($_SERVER["PHP_SELF"]) . '/'));

// expire session if last activity was over 1hr ago (in seconds)
define('MAX_SESSION_LENGTH', 3600);
if (isset($_SESSION['lastActivity']) && (time() - $_SESSION['lastActivity'] > MAX_SESSION_LENGTH)){
    // last request was more than 60 minutes ago
    session_unset();     // unset $_SESSION variable for the run-time
    session_destroy();   // destroy session data in storage

    session_start();
    $_SESSION['systemMessage'] = [['warning', 'Your session has timed out due to inactivity. Please sign in again.']];

    header('Location: ' . ROOT_DIR_PATH . 'index.php');
    exit;
}
$_SESSION['lastActivity'] = time(); // update last activity time stamp

// including DB since most pages use it
require_once 'dbConnector.php';

// see if any users are logged in and/or admins
$isUserLoggedIn = false;
$adminUserExists = false;
if (isset($_SESSION['user']) && is_array($_SESSION['user']) && count($_SESSION['user']) > 0){
    $isUserLoggedIn = true;

    for ($i = 0; $i < count($_SESSION['user']); $i++){
        if ($_SESSION['user'][$i]['admin'] == 1){
            $adminUserExists = true;
            break;
        }
    }
}

// set up some common functions
function test_input($data) {
    $data = trim($data);
    $data = stripslashes($data);
    $data = htmlspecialchars($data);

    return $data;
}
function test_dup($fld, $fldVal){
    $query = DB::queryFirstRow("SELECT * FROM users where $fld = %s", $fldVal);

    return $query;
}
function convert_or_keep_number_to_word($val){
    $dict = [
        0 => 'zero',
        1 => 'one',
        2 => 'two',
        3 => 'three',
        4 => 'four',
        5 => 'five',
        6 => 'six',
        7 => 'seven',
        8 => 'eight',
        9 => 'nine'
    ];
    if (($val * 1) < 10){
        return $dict[$val];
    }
    else {
        return $val;
    }
}

if (!isset($_SESSION['systemMessage'])){
    $_SESSION['systemMessage'] = [];
}

$tmpArray = explode('/', preg_replace('/\.php$/', '', $_SERVER["PHP_SELF"]));
// set up shortname (filename of URL w/o extension)
define('SCRIPT_BASE_NAME', $tmpArray[count($tmpArray) - 1]);

$pagesNeedingLogin = ['quiz','results','admin','addQuestion','listQuestions','editQuestion', 'editQuestionPost'];
if (in_array(SCRIPT_BASE_NAME, $pagesNeedingLogin) && !$isUserLoggedIn){
    $_SESSION['systemMessage'] = [['warning', 'Please sign in before viewing that page.']];
    $_SESSION['userIdx'] = 0;

    header('Location: ' . ROOT_DIR_PATH . 'index.php');
    exit;
}
$pos = strpos(dirname($_SERVER["PHP_SELF"]), '/admin/');

// set the max number of players that can play
define('QUIZ_MAX_PLAYERS', 2);
define('QUIZ_MAX_QUESTIONS', 10);
?>