<?php
// $questionID is set if the user simply reloaded the page.  if so, get the same question as asked previously
if (isset($questionID) AND $questionID > 0){
    $questionDB = DB::queryFirstRow('SELECT * FROM question WHERE question_id = %i', $questionID);
}
// otherwise, an array of questionIDs was saved into the questionID session var
else {
    // since users start with an empty array, handle that instance here
    if (count($_SESSION['questionIDs']) == 0){
        $questionIDs = [0];
    }
    // otherwise set $questionIDs to session var
    else {
        $questionIDs = $_SESSION['questionIDs'];
    }

    // now get the question for the user
    $questionDB = DB::queryFirstRow('SELECT * FROM question WHERE level_id = %i AND question_id NOT IN %li AND topic_area_id IN %li ORDER BY RAND() LIMIT 1', $_SESSION['gradelevel'], $questionIDs, $_SESSION['topics']);

}

// save query response
$questionID = $questionDB['question_id'];
$question = $questionDB['question'];
$answerInfo = $questionDB['right_wrong_desc'];
$questionType = $questionDB['question_type_id'];

// add this question to the user's questionIDs array
if (!in_array($questionID, $_SESSION['user'][$_SESSION['userIdx']]['questionIDs'])){
    // if user was asked too many questions, goto results
    if (count($_SESSION['user'][$_SESSION['userIdx']]['questionIDs']) >= QUIZ_MAX_QUESTIONS){
        header('Location: results.php');
        exit;
    }

    array_push($_SESSION['user'][$_SESSION['userIdx']]['questionIDs'], $questionID);
}

switch($questionType){
    // multiple choice
    case 1:
        // randomize order, but alter query to make sure "all of the above" answer is always last
        $answersDB = DB::query("SELECT a.*, RAND() AS display_order FROM answer a WHERE question_id = %i AND LOWER(answer) NOT LIKE '%all of the above%' UNION SELECT a.*, -1 AS display_order FROM answer a WHERE question_id = %i AND LOWER(answer) LIKE '%all of the above%' ORDER BY display_order DESC", $questionID, $questionID);

        $answers = array();
        foreach ($answersDB as $row) {
            array_push($answers, $row['answer']);
            if($row['correct'] == 'Y') {
                $correctAnswer = max(array_keys($answers));
            }
        }

        break;

    // true or false
    case 2:
        $correctAnswer = $questionDB['tf_ans'];

        break;

    // sort list
    case 3:
        $answersDB = DB::query("SELECT a.* FROM answer a WHERE question_id = %i ORDER BY RAND() ASC", $questionID, $questionID);

        $answers = [0, 0, 0, 0];
        foreach ($answersDB AS $idx => $key){
            $newIdx = intval($answersDB[$idx]["correct"]) - 1;
            $answers[$newIdx] = $answersDB[$idx]["answer_id"];
        }
        // save as comma-delimited list of answer_id's in the correct order
        $correctAnswer = implode(",", $answers);

        break;
}
?>