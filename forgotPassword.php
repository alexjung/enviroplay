<?php
require_once 'includes/utilityFunctions.php';

$email = "";
$emailErr = "";
$emailNotValid = true;
$valid = true;

if ($_SERVER["REQUEST_METHOD"] == "POST") {

    if (empty($_POST["email"])) {
        array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Email is requried.']);
        $valid = false;
    } else {
        $email = test_input($_POST["email"]);
        $emailNotValid = test_dup('email', $email);
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Email is requried.']);
            $valid = false;
        }
        elseif ( is_null($emailNotValid) ) {
            array_push($_SESSION['systemMessage'], ['danger', '<strong>Error:</strong> Email does not exist.']);
            $emailErr = "Email does not exist<br/>";
            $valid = false;
        }
    }

    if($valid) {
        $_SESSION['email'] = $_POST["email"];
        header('Location: includes/_forgotPasswordEmail.php');
    }
}

$pageTitle = "Forgot Password";

require_once 'includes/template/header.php';
?>

<div class="container" role="main">
    <div class="well well-lg clearfix">
        <h3><?php echo $pageTitle; ?></h3>

        <p>Please enter the email address associated with your account and we will send you an email with your credentials.</p>

        <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>" method="post" class="form-inline">
            <div class="form-group">
                <label for="inputEmail">Email:</label>
                <input type="email" class="form-control" name="email" id="email" placeholder="Email">
            </div>

            <button type="submit" class="btn btn-default">Submit</button> <a href="index.php"><button type="button" class="btn btn-lg btn-link">Cancel</button></a>
        </form>
    </div>
</div>

<?php
require_once 'includes/template/footer.php';
?>
